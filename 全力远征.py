import DollFrontline as DF
import numpy as np
import time
import datetime
import threading


# if __name__ == "__main__":
df = DF.DollFrontlineBase(False)

	
def process_restart():
	global restart_timer
	print("Restart time reached. Restarting...")
	df.restart_app()
	restart_timer = threading.Timer(4 * 3600 + 10 * 60, process_restart)
	restart_timer.start()
	

while True:
	if df.confirm("闪退后", "state_confirm_1"):
		if df.process_StartUp2() == False:
			continue
	else:
		if df.restart_app() == False:
			break
	restart_timer = threading.Timer(4 * 3600 + 10 * 60, process_restart)
	restart_timer.start()
	
	df.do_simulation()
	
	df.back_to_base()
	update_support_time_success = False
	
	while not update_support_time_success:
		df.处理远征()
		update_support_time_success = df.update_support_time()
	
	while True:
		if datetime.time(5, 0, 0, 0) < datetime.datetime.now().time() < datetime.time(10, 0, 0, 0):
		# if datetime.time(20, 10, 0, 0) < datetime.datetime.now().time() < datetime.time(20, 40, 0, 0):
			now = datetime.datetime.now().time()
			_tmp = datetime.timedelta(hours=now.hour, minutes=now.minute, seconds=now.second)
			# sleep_sec = (datetime.timedelta(hours=10)).seconds
			# sleep_sec = (datetime.timedelta(hours=now.hour, minutes=40, seconds=now.second) - _tmp).seconds
			sleep_sec = (datetime.timedelta(hours=10, minutes=0, seconds=0) - _tmp).seconds
			print("sleep ", sleep_sec, " seconds totally.")
			df.close_app()
			restart_timer.cancel()
			for i in range(20):
				print("sleep ", i, " times.")
				time.sleep(sleep_sec / 20)
			print("It's 10 clock, it's time to wake up.")
			break
		
		_time_array = np.array(df.SupportRemainTime)
		try:
			time.sleep(_time_array[_time_array > 0].min() - 30)
		except:
			pass
		while not df.SupportReturn:
			time.sleep(1)
		if df.SupportReturn:
			df.SupportReturn = False
			update_support_time_success = False
			while not update_support_time_success:
				df.处理远征()
				update_support_time_success = df.update_support_time()
		if df.TimeToSimulation == True:
			df.TimeToSimulation = False
			df.do_simulation()
			
			df.SupportReturn = False
			update_support_time_success = False
			while not update_support_time_success:
				df.处理远征()
				update_support_time_success = df.update_support_time()
