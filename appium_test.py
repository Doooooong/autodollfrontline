import appium.common
import appium.webdriver
import subprocess as sb
import time


# adb 命令： dumpsys activity | grep mFocusedActivity
desired_caps = {
"platformName": "Android",
"deviceName": "Phone",
"fullReset": "False",
"no-reset": "True",
"newCommandTimeout": "86400",
}

# def attach_to_session(executor_url, session_id):
#     original_execute = WebDriver.execute
#     def new_command_execute(self, command, params=None):
#         if command == "newSession":
#             # Mock the response
#             return {'success': 0, 'value': None, 'sessionId': session_id}
#         else:
#             return original_execute(self, command, params)
#     # Patch the function before creating the driver object
#     WebDriver.execute = new_command_execute
#     driver = webdriver.Remote(command_executor=executor_url, desired_capabilities={})
#     driver.session_id = session_id
#     # Replace the patched function with original function
#     WebDriver.execute = original_execute
#     return driver

# sb.check_output(["C:\\Program Files (x86)\\Android\\android-sdk\\platform-tools\\adb.exe", "connect", "localhost:7555"])
# time.sleep(2)
# self.session_id = "945f52d1bb5a427b8bc03adadb2cf1cc"
# # sb.check_output(["C:\\Program Files\\Appium\\appium", "-a", "localhost", "-p", "4723", "--session-override"])
# self.server = 'http://localhost:4723/wd/hub'  #
# self.driver = appium.webdriver.Remote(self.server, desired_caps)
# # self.driver.session_id = self.session_id
# # self.driver.get(self.server)
# self.touch = ta.TouchAction(self.driver)
# time.sleep(2)

# self.driver.swipe(pos1[0], pos1[1], pos2[0], pos2[1], drag_time)

# def drag_wayPoint(self, wayPoints):
# 	__tmp = self.touch.press(x=wayPoints[0][0], y=wayPoints[0][1]).wait(5000)  # .perform()
# 	time.sleep(0.003)
# 	for wayPoint in wayPoints:
# 		__tmp = __tmp.move_to(x=wayPoint[0], y=wayPoint[1]).wait(100)  # .perform()
# 	__tmp.perform().release()
# 	drag_time = MouseOperation.DRAG_TIME + self.get_rand_at_range(-MouseOperation.DRAG_TIME_RANGE,
# 	                                                              MouseOperation.DRAG_TIME_RANGE)
# 	time.sleep(drag_time / 1000.0)

def connect_to_simulator():
	sb.check_output(["C:\\Program Files (x86)\\Android\\android-sdk\\platform-tools\\adb.exe", "connect", "localhost:7555"])
	server = 'http://localhost:4723/wd/hub'  #
	driver = appium.webdriver.Remote(server, desired_caps)
	return driver
