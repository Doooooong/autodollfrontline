import cv2
import numpy as np
# import tesserocr
import pytesseract
from PIL import Image
import MakeRECT
pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"


def compare_image(source, template, rect=None, threshold=0.87):
	return cv2.matchTemplate(source, template, cv2.TM_CCOEFF_NORMED).max()

def compare_hsv_image(source, template, rect=None, threshold=0.87):
	source_h = cv2.cvtColor(source, cv2.COLOR_BGR2HSV)[:, :, 0]
	template_h = cv2.cvtColor(template, cv2.COLOR_BGR2HSV)[:, :, 0]
	MakeRECT.cv2_imshow("src", source_h)
	MakeRECT.cv2_imshow("tmp", template_h)
	return cv2.matchTemplate(source_h, template_h, cv2.TM_CCOEFF_NORMED).max()

def find_image(source, template, rect=None, threshold=0.87):
	epsion = 1e-10
	res = cv2.matchTemplate(source, template, cv2.TM_CCOEFF_NORMED)
	loc = np.where(res == res.max())
	return [loc[1][0], loc[0][0]]


def text_ocr(img, lang='chi_sim', char_whitelist='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIGKLMNOPQRSTUVWXYZ', psm_para="6", padding=0, watch=False, threshold=180, erode=False, inverse=False, iterations=1):
	# binnary = img
	_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
	ret, binnary = cv2.threshold(_gray, threshold, 255, cv2.THRESH_OTSU)
	kernel = np.zeros((3, 3), dtype=np.uint8)
	kernel[1, :] = np.ones((1, 3))
	kernel[:, 1] = np.ones(3)
	if erode:
		binnary = cv2.erode(binnary, kernel, iterations=iterations)
	else:
		binnary = cv2.dilate(binnary, kernel, iterations=iterations)

	if inverse:
		binnary[binnary > 100] = 100
		binnary[binnary < 100] = 255
		binnary[binnary == 100] = 0
		# cv2.invert(binnary, binnary)
	if padding:
		binnary = cv2.copyMakeBorder(binnary, padding, padding, padding, padding, cv2.BORDER_CONSTANT, None, 0)
	
	if watch:
		MakeRECT.cv2_imshow("ocr", binnary)
	
	ocr = Image.fromarray(binnary)
	if char_whitelist != "":
		config_str = "-c tessedit_char_whitelist=" + char_whitelist + " "  #  + "-psm 6 "
	else:
		config_str = "-c"
	
	if psm_para != "":
		config_str += "--psm " + psm_para + " "
	else:
		config_str += "--psm 6 "

	if lang == '':
		text = pytesseract.image_to_string(ocr, config=config_str)
	else:
		text = pytesseract.image_to_string(ocr, lang=lang, config=config_str)

	return text



	# orb = cv2.ORB_create()
	# # sift = cv2.xfeatures2d.SIFT_create()
	# surf = cv2.xfeatures2d.SURF_create()
	# alg = surf
	# if rect is None:
	# 	keypoint1, descriptor1 = alg.detectAndCompute(source, None)
	# else:
	# 	__tmp = source[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2], :]
	# 	keypoint1, descriptor1 = alg.detectAndCompute(__tmp, None)
	#
	# keypoint2, descriptor2 = alg.detectAndCompute(template, None)
	#
	# # 获得一个暴力匹配器的对象
	# bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
	# # 利用匹配器 匹配两个描述符的相近成都
	# matches = bf.knnMatch(descriptor1, descriptor2, k=1)
	#
	# img3 = cv2.drawMatchesKnn(source, keypoint1, template, keypoint2, matches, template, flags=2)
	# cv2.imshow("matches", img3)
	# cv2.waitKey()
	# cv2.destroyAllWindows()
	#
	#
	# distance = 0
	# for res in matches:
	# 	distance += res.distance
	#
	# return distance
