import DollFrontline as DF
import time
import datetime as dt

全部打手 = ["FAL", "G11", "416", "Zas", "SOPMODII", "M4A1", "AR15"]
TIMES = 12  # 16*20  12-240000exp
打手 = ["Zas", "Zas"]
打手位置 = 5
NUM_BATTLE = 5
st_time = dt.datetime.now()
df = DF.DollFrontlineBase()
df.AutoExtension = False  # 关闭自动扩编


UseRetryButton = False
while TIMES > 0:
    NeedReturnToBase = False
    while df.getState() == ['']:
        df.op.rnd_sleep(100, 30)
    # for state_str in df.state:
    df.op.rnd_sleep(800, 80)
    if df.getState("战斗选择界面_作战")[0] == "战斗选择界面_作战":
        NeedReturnToBase = False
    elif df.getState("8_1夜战放大")[0] == '8_1夜战放大':
        pass
    # break
    else:
        NeedReturnToBase = True
    if NeedReturnToBase or df.SupportReturn:
        df.SupportReturn = False
        df.back_to_base()
        df.op.rnd_sleep(1000, 30)
        df.update_support_time()
        df.op.rnd_sleep(1000, 30)
        df.back_to_base()
        df.op.rnd_sleep(1000, 30)
        df.click_Battle()
    res_enter_map = True
    if df.getState("8_1夜战放大")[0] != '8_1夜战放大':
        UseRetryButton = False
        res_enter_map = df.select_enter_map(8, 1, "N")
    if res_enter_map == False:
        continue
    # if not  continue
    if df.click_分解() == True:
        df.send_msg_to_phone()
        while True:
            time.sleep(100)
    if UseRetryButton == False:
        df.op.rnd_sleep(2000, 500)

        if df.isZoomOuted[0][2][0] == False:
            if df.confirm("8_1夜战放大", "zoom_confirm") == True:
                df.send_msg_to_phone()
                input("Zoom out, and enter anything to continue: ")
                continue
            df.isZoomOuted[0][2][0] = True

    while df.getState() == []:
        df.op.rnd_sleep(100, 30)
    df.op.rnd_sleep(1500, 300)
    df.click("作战地图8_1_N", "btn_练级队伍", [2000, 400])
    fighter_name = df.find_fighter(打手, 打手位置)
    df.click("战斗梯队部署界面", "btn_编成", [1800, 400])

    while df.getState() == []:
        df.op.rnd_sleep(100, 30)
    df.op.rnd_sleep(1000, 500)  # 等待人物槽位加载出来
    fighter_HP = df.get_HP(打手位置)
    print("Fighter HP is: ", fighter_HP)
    df.change_doll(打手位置, fighter_name, ["AR"], ["5"])
    while df.getState("编成界面") == ["编成界面"]:
        df.click("编成界面", "btn_Return", [600, 400])
        df.op.rnd_sleep(100, 30)

    # 部署练级队伍
    while df.getState("作战地图8_1_N") == []:
        df.op.rnd_sleep(100, 30)
    df.op.rnd_sleep(1000, 30)
    df.click_ex("作战地图8_1_N", "btn_练级队伍", [1200, 400], stuck_mode=[False, True]
                , after_confirm=["战斗梯队部署界面", "state_confirm_1", None, True])
    if fighter_HP < 35:
        df.repair_battle(打手位置)
    df.click_ex("战斗梯队部署界面", "btn_Confirm", [1200, 400], stuck_mode=[False, True],
                after_confirm=["作战地图8_1_N", "state_confirm_1", None, True])
    # 部署补给梯队
    df.click_ex("作战地图8_1_N", "btn_补给队伍", [1200, 400], stuck_mode=[False, True],
                after_confirm=["战斗梯队部署界面", "state_confirm_1", None, True])
    df.click_ex("战斗梯队部署界面", "btn_Confirm", [1200, 400], stuck_mode=[False, True],
                after_confirm=["作战地图8_1_N", "state_confirm_1", None, True])

    # 开始战斗
    df.click_ex("作战地图8_1_N", "btn_BattleStart", [2000, 400], stuck_mode=[False, True], click_cnt=1,
                after_confirm=["作战地图0_2", "btn_PlanExec", "btn_TurnEnd", True], loop_duty=2000)

    df.op.rnd_sleep(1 * 1000, 0)

    # 补给
    df.click("作战地图8_1_N", "btn_补给队伍", [700, 200])  # 选中梯队
    df.click_ex("作战地图8_1_N", "btn_补给队伍", [1400, 200], after_confirm=["战斗梯队部署界面", "btn_Confirm", None, True])  # 弹出补给界面
    df.click_ex("战斗梯队部署界面", "btn_补给", [1400, 200], after_confirm=["作战地图8_1_N", "state_confirm_1", None, True])  # 确定补给

    # 撤退补给队伍 撤退确认界面
    df.click("作战地图8_1_N", "btn_补给队伍", [700, 200])  # 选中梯队
    df.click_ex("作战地图8_1_N", "btn_补给队伍", [1400, 200], after_confirm=["战斗梯队部署界面", "btn_Confirm", None, True])  # 弹出补给界面
    df.click_ex("战斗梯队部署界面", "btn_Cancel", [1400, 200], after_confirm=["作战地图8_1_N", "state_confirm_1", None, True])  # 点击撤退
    df.click("撤退确认界面", "btn_confirm", [1400, 200])  # 点击撤退

    # 设置计划
    df.click("作战地图8_1_N", "btn_PlanMode", [700, 200])
    df.click("作战地图8_1_N", "btn_练级队伍", [700, 200])  # 选中练级部队
    df.click("作战地图8_1_N", "btn_途经点1", [700, 200])
    df.click("作战地图8_1_N", "btn_途经点2", [700, 200])
    df.click("作战地图8_1_N", "btn_PlanExec", [700, 200])

    # df.op.rnd_sleep(140 * 1000, 0)
    battle_count = 0
    while True:
        # 战斗之间
        while not df.confirm("战斗中", "state_confirm_1"):
            df.op.rnd_sleep(2000, 300)
        # df.op.rnd_sleep(5000, 400)

        # 战斗中
        while df.confirm("战斗中", "state_confirm_1"):
            df.op.rnd_sleep(700, 300)

        battle_count += 1
        # 战斗结束，快速点击屏幕
        for i in range(9):
            df.click_ex("战斗中", "btn_click", delay=[130, 30])
        if battle_count >= NUM_BATTLE:
            break

    while not df.confirm("作战地图8_1_N", "btn_PlanMode", capture_rect_scale=1.05):
        df.op.rnd_sleep(2 * 1000, 0)

    df.click_ex("作战地图8_1_N", "btn_BattleEnd", after_confirm=["作战终止界面", "state_confirm_1", None, True], delay=[500, 30])
    if TIMES > 0:  # 点击Retry
        UseRetryButton = True
        df.click_ex("作战终止界面", "btn_retry", after_confirm=["作战终止界面", "state_confirm_1", None, False], delay=[2000, 300])
    else:  # 结束战斗
        df.click_ex("作战终止界面", "btn_end", after_confirm=["作战终止界面", "state_confirm_1", None, False], delay=[2000, 300])


    print("Times: ", TIMES, " finished!")
    print("======================================")
    TIMES -= 1
    df.stop_timeout = 0

df.send_msg_to_phone()
df.stop_timeout = -9999999999999

print(dt.datetime.now() - st_time)
