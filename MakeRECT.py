import pywin32HelperFunc as win32
import cv2
from PIL import Image, ImageDraw, ImageFont
import numpy as np
import json
import os
import re
import copy

def PIL_putUnicodeText(image, text, pos, font, color):
	img_PIL = Image.fromarray(image)
	# img_PIL = Image.fromarray(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
	# font = ImageFont.truetype('simhei.ttf', size)
	fillColor = color  # 颜色
	position = pos  # 位置
	if not isinstance(text, str):
		text = text.decode('utf-8')
	draw = ImageDraw.Draw(img_PIL)
	draw.text(position, text, font=font, fill=fillColor)
	img = np.asarray(img_PIL)
	# img = cv2.cvtColor(np.asarray(img_PIL), cv2.COLOR_RGB2BGR)
	return img


def PIL_getTextSize(text, font):
	img_PIL = Image.fromarray(np.zeros((10, 10, 4), dtype='uint8'))
	draw = ImageDraw.Draw(img_PIL)
	width, height = draw.textsize(text, font)
	# height = font.size
	return width, height
	

def opencv_draw_rectangle_withText(org_img, rect, color_rgb=(255, 0, 0), text=None):
	font_scale = 1.5
	font = cv2.FONT_HERSHEY_PLAIN
	PIL_font_size = 15
	PIL_font = ImageFont.truetype('msyhbd.ttc', PIL_font_size)
	
	x = rect[0]
	y = rect[1]
	w = rect[2]
	h = rect[3]

	img = copy.deepcopy(org_img)
	
	rectangle_bgr = (color_rgb[2], color_rgb[1], color_rgb[0])
	cv2.rectangle(img, (x, y), (x + w, y + h), rectangle_bgr, thickness=1)

	if text is not None and text.__len__() > 0:
		text_bgr = (255 - color_rgb[2], 255 - color_rgb[1], 255 - color_rgb[0])
		text_rectangle_bgr = rectangle_bgr
		text_rectangle_coord = [[0, 0], [0, 0]]
		margin = 5
		# (text_w, text_h) = cv2.getTextSize(text, font, fontScale=font_scale, thickness=1)[0]
		(text_w, text_h) = PIL_getTextSize(text, PIL_font)
		text_rectangle_w = text_w + margin * 2
		text_rectangle_h = text_h + margin * 2

		if y - text_h < 0:
			text_rectangle_coord[0][1] = y + h + text_rectangle_h
			text_rectangle_coord[1][1] = y + h
		else:
			text_rectangle_coord[0][1] = y
			text_rectangle_coord[1][1] = y - text_rectangle_h
			
		if x + text_w > 1920:
			text_rectangle_coord[0][0] = x + w - text_rectangle_w
			text_rectangle_coord[1][0] = x + w
		else:
			text_rectangle_coord[0][0] = x
			text_rectangle_coord[1][0] = x + text_rectangle_w
		
		text_pos = (text_rectangle_coord[0][0] + margin, text_rectangle_coord[1][1] + margin)

		pt1 = tuple(text_rectangle_coord[0])
		pt2 = tuple(text_rectangle_coord[1])
		cv2.rectangle(img, pt1, pt2, text_rectangle_bgr, cv2.FILLED)
		# cv2.putText(img, text, text_pos, font, fontScale=font_scale, color=text_bgr, thickness=2)
		img = PIL_putUnicodeText(img, text, text_pos, PIL_font, text_bgr)
		
	return img



def capture_make_roi():
	global cap
	ROI_dict = {}
	
	# Capture image.
	_image = cv2.cvtColor(cap.capture(), cv2.COLOR_RGBA2RGB)
	
	# Save the origin image.
	print("Input the name of this screenshots: ")
	_img_file_name = input()
	cv2.imencode('.png', _image)[1].tofile(".\\fig\\org_" + _img_file_name + ".png")
	
	# Loop and set ROIs.
	while True:
		# cv2.namedWindow("res", cv2.WINDOW_KEEPRATIO)
		# cv2.imshow("res", _image)
		# _ROI = cv2.selectROI("res", _image)
		_ROI = cv2_imshow("Make ROIs", _image, setupROI=True)
		cv2.destroyWindow("res")
		if _ROI == (0, 0, 0, 0):  # If set nothing EXIT.
			break
			
		print("Input the name of ROI: ", _ROI, " :")
		_name = input()
		
		while True:
			try:
				ROI_dict[_name]
			except KeyError:
				ROI_dict.update({_name: _ROI})
				_image = opencv_draw_rectangle_withText(_image, _ROI, text=_name)
				break
			print("Name:\"", _name, "\" is duplicated! Re-input the name of ROI: ", _ROI, " :")
			_name = input()
			
	# Save json file and image with set ROIs.
	cv2.imencode('.png', _image)[1].tofile(".\\fig\\roi_" + _img_file_name + ".png")
	json_save(".\\fig\\roi_" + _img_file_name + ".json", ROI_dict)
	
	
def json_save(path, dict):
	file = open(path, 'w')
	file.write(json.dumps(dict))


def update_roi_image(names, ROI_dict, image_dict, file_path=".\\fig\\"):
	for name in names:
		_img = image_dict[name]
		for roi_name in ROI_dict[name]:
			_img = opencv_draw_rectangle_withText(_img, ROI_dict[name][roi_name], text=roi_name)

		cv2.imencode('.png', _img)[1].tofile(file_path + "roi_" + name + ".png")


def show_roi_image(name, ROI_dict, image_dict, file_path=".\\fig\\"):
	_img = image_dict[name]
	for roi_name in ROI_dict[name]:
		_img = opencv_draw_rectangle_withText(_img, ROI_dict[name][roi_name], text=roi_name)
		
	cv2.namedWindow(zh_ch("roi_" + name), cv2.WINDOW_KEEPRATIO)
	cv2.imshow(zh_ch("roi_" + name), _img)
	ret = cv2.waitKey(0)
	cv2.destroyWindow(zh_ch("roi_" + name))
	return "%c" % ret

	
def read_data(file_path=".\\fig\\"):
	
	ROI_dict = {}
	image_dict = {}
	name_list = []
	for file in os.listdir(file_path):
		if os.path.splitext(file)[-1] == '.png':
			name = re.split('org_|.png', file)[1]
			if name == '':
				continue
			name_list.append(name)
			jsonfile_name = 'roi_' + name + '.json'
			
			try:
				json.load(open(file_path + jsonfile_name, 'r'))
				ROI_dict.update({name: json.load(open(file_path + jsonfile_name, 'r'))})
			except FileNotFoundError:
				json_save(file_path + jsonfile_name, {})
				ROI_dict.update({name: {}})
			except:
				pass
			image = cv2.imdecode(np.fromfile(file_path + "org_" + name + ".png", dtype=np.uint8), cv2.IMREAD_COLOR)
			image_dict.update({name: image})
	
	ROI_confirm_dict = json.load(open(file_path + "confirm\\confirm.json", 'r'))
	# try:
	# 	ROIs_confirm_dict[name][roi_name].append(roi_name)
	# except KeyError:
	# 	ROIs_confirm_dict[name].update({roi_name: [roi_name]})
	ROI_confirm_image_dict = {}
	for scene_name in ROI_confirm_dict:
		# try:
		try:
			ROI_confirm_image_dict[scene_name]
		except KeyError:
			ROI_confirm_image_dict.update({scene_name: {}})
		for roi_name in ROI_confirm_dict[scene_name]:
			try:
				ROI_confirm_image_dict[scene_name][roi_name]
			except KeyError:
				ROI_confirm_image_dict[scene_name].update({roi_name: {}})
				
			for name in ROI_confirm_dict[scene_name][roi_name]:
				__path = file_path + '\\confirm\\' + scene_name + "\\confirm_" + name + ".png"
				image = cv2.imdecode(np.fromfile(__path, dtype=np.uint8), cv2.IMREAD_COLOR)
				
				try:
					ROI_confirm_image_dict[scene_name][roi_name][name]
				except KeyError:
					ROI_confirm_image_dict[scene_name][roi_name].update({name: None})
				
				ROI_confirm_image_dict[scene_name][roi_name].update({name: image})
				# ROI_confirm_image_dict.update({scene_name: {roi_name: {name: image}}})
		# except KeyError:
		# 	pass
	# for root, dirs, files in os.walk(file_path + '\\confirm'):
	# 	for name in dirs:
	# 		print(name)
	return name_list, ROI_dict, image_dict, ROI_confirm_dict, ROI_confirm_image_dict


def point_in_rect(point, rect):
	return point[0] > rect[0] and point[1] > rect[1] and point[0] < rect[0] + rect[2] and point[1] < rect[1] + rect[3]


def getPointInWhichRect(point, rect_dict):
	for name in rect_dict:
		if point_in_rect(point, rect_dict[name]):
			print("[", name, "] is you point to?")
			ret = input("")
			if ret == 'n':
				continue
			return name
	return None
	
	
def on_EVENT_LBUTTONDOWN(event, x, y, flags, param):
	if event == cv2.EVENT_LBUTTONDOWN:
		param[0] = x
		param[1] = y
	
	
def zh_ch(string):
	return string.encode('gbk').decode("utf-8", errors='ignore')


def cv2_imshow(caption, img, mouse_callback=None, param=None, setupROI=False):
	cv2.namedWindow(caption, cv2.WINDOW_KEEPRATIO)
	if mouse_callback is not None:
		cv2.setMouseCallback(caption, mouse_callback, param)
	cv2.imshow(caption, img)
	if setupROI == True:
		_ROI = cv2.selectROI(caption, img)
	else:
		try:
			ret = "%c" % cv2.waitKey(0)
		except:
			ret = ""
	cv2.destroyWindow(caption)
	if setupROI == True:
		return _ROI
	else:
		return ret

	
def save_confirm_image(image, name, ROIs, ROIs_confirm_dict):
	'''
	:param name:
	:param ROIs:
	:param ROIs_confirm_dict:
	:return:
	'''
	while True:
		input_continue = input("Continue " + name + "? ")
		if input_continue == 'n':
			return
		
		while True:
			input_new_capture = input("Capture a new image?")
			if input_new_capture != 'n':
				org_image = cv2.cvtColor(cap.capture(), cv2.COLOR_RGBA2RGB)
			else:
				org_image = copy.deepcopy(image)
			_image = np.array(org_image)
			for roi_name in ROIs[name]:
				_image = opencv_draw_rectangle_withText(_image, ROIs[name][roi_name], text=roi_name)
			cv2_imshow("Image is right?", _image)
			input_new_capture = input("Image is right?")
			if input_new_capture == 'y' or input_new_capture == 'Y':
				break
				
		pos = [-1, -1]
		
		cv2_imshow("Select a rect", _image, on_EVENT_LBUTTONDOWN, pos)
		
		if pos[0] == -1 or pos[1] == -1:
			print("No point clicked, continue!")
			continue
		
		roi_name = getPointInWhichRect((pos[0], pos[1]), ROIs[name])
		roi = ROIs[name][roi_name]
		save_image = org_image[roi[1]: roi[1] + roi[3], roi[0]: roi[0] + roi[2], :]
		
		cv2_imshow("选择的 rect", save_image)
		
		print("Default name is : ", roi_name)
		tmp_name = input("Input a new name (or input nothing to use default name): ")
		if tmp_name == "":
			try:
				cv2.imencode('.png', save_image)[1].tofile(".\\fig\\confirm\\" + name + "\\confirm_" + roi_name + ".png")
			except FileNotFoundError:
				os.mkdir(".\\fig\\confirm\\" + name)
				cv2.imencode('.png', save_image)[1].tofile(".\\fig\\confirm\\" + name + "\\confirm_" + roi_name + ".png")
			print("Name of saved picture is : ", "\\confirm_" + roi_name + ".png")
			try:
				ROIs_confirm_dict[name][roi_name].append(roi_name)
			except KeyError:
				ROIs_confirm_dict[name].update({roi_name: [roi_name]})
		else:
			tmp_name = re.split('confirm_|.png', tmp_name)[0]
			try:
				cv2.imencode('.png', save_image)[1].tofile(".\\fig\\confirm\\" + name + "\\confirm_" + tmp_name + ".png")
			except FileNotFoundError:
				os.mkdir(".\\fig\\confirm\\" + name)
				cv2.imencode('.png', save_image)[1].tofile(".\\fig\\confirm\\" + name + "\\confirm_" + tmp_name + ".png")
			print("Name of saved picture is : ", "confirm_" + tmp_name + ".png")
			try:
				ROIs_confirm_dict[name][roi_name].append(tmp_name)
			except KeyError:
				ROIs_confirm_dict[name].update({roi_name: [tmp_name]})
		
		pass
	# btn_TurnEnd btn_PlanMode_Click


def adding_new_rois(org_img, roi_dict, name):
	
	_img = org_img
	while True:
		for roi_name in roi_dict:
			_img = opencv_draw_rectangle_withText(_img, roi_dict[roi_name], text=roi_name)
		_ROI = cv2_imshow("update_rois", _img, setupROI=True)
		if _ROI == (0, 0, 0, 0):  # If set nothing EXIT.
			break
		
		print("Input the name of ROI: ", _ROI, " :")
		_name = input()
	
		while True:
			try:
				roi_dict[_name]
			except KeyError:
				roi_dict.update({_name: _ROI})
				_img = opencv_draw_rectangle_withText(_img, _ROI, text=_name)
				break
			print("Name:\"", _name, "\" is duplicated! Re-input the name of ROI: ", _ROI, " :")
			print("Input [Y] for (default): Re-input the name of ROI.")
			print("Input [N] for: Replace the old roi.")
			user_input = input()
			if user_input in ['Y', 'y', '']:
				_name = input()
			else:
				roi_dict.update({_name: _ROI})
				_img = opencv_draw_rectangle_withText(_img, _ROI, text=_name)
				break
	
	# Save json file and image with set ROIs.
	cv2.imencode('.png', _img)[1].tofile(".\\fig\\roi_" + name + ".png")
	json_save(".\\fig\\roi_" + name + ".json", roi_dict)

def update_rois(org_img, roi_dict, name):

	input_new_capture = input("Capture a new image?")
	if input_new_capture != 'n':
		org_img = cv2.cvtColor(cap.capture(), cv2.COLOR_RGBA2RGB)

	_img = copy.deepcopy(org_img)
	while True:
		input_continue = input("Continue " + name + "? ")
		if input_continue == 'n':
			break

		for roi_name in roi_dict.keys():
			_img = opencv_draw_rectangle_withText(_img, roi_dict[roi_name], text=roi_name)

		pos = [-1, -1]

		cv2_imshow("Select a rect", _img, on_EVENT_LBUTTONDOWN, pos)

		if pos[0] == -1 or pos[1] == -1:
			print("No point clicked, continue!")
			continue

		roi_name = getPointInWhichRect((pos[0], pos[1]), ROIs[name])

		while True:
			_ROI = cv2_imshow("update_rois", org_img, setupROI=True)
			roi_dict[roi_name] = _ROI
			_img = opencv_draw_rectangle_withText(org_img, roi_dict[roi_name], text=roi_name)
			cv2_imshow("UPDATED_rois", _img)
			_sel = input("Is the new ROI is right?")
			if _sel == "y" or _sel  == "Y":
				break

	# Save json file and image with set ROIs.
	cv2.imencode('.png', _img)[1].tofile(".\\fig\\roi_" + name + ".png")
	json_save(".\\fig\\roi_" + name + ".json", roi_dict)


def replace_base_image(org_img, roi_dict, name):
	while True:
		_sel = input("The capture is right?")
		if _sel == "y" or _sel == "Y":
			break

		org_image = cv2.cvtColor(cap.capture(), cv2.COLOR_RGBA2RGB)
		roi_img = copy.deepcopy(org_image)
		for roi_name in roi_dict.keys():
			roi_img = opencv_draw_rectangle_withText(roi_img, roi_dict[roi_name], text=roi_name)
		cv2_imshow("Confirm the capture", roi_img)

	# Save json file and image with set ROIs.
	cv2.imencode('.png', roi_img)[1].tofile(".\\fig\\roi_" + name + ".png")
	cv2.imencode('.png', org_image)[1].tofile(".\\fig\\org_" + name + ".png")
	json_save(".\\fig\\roi_" + name + ".json", roi_dict)

if __name__ == "__main__":
	pass
	# cap = win32.Win32_Capture("ドルフロ - MuMu模拟器", "NemuPlayer")
	# cap = win32.Win32_Capture("夜神模拟器", "ScreenBoardClassWindow")
	cap = win32.Win32_Capture("少女前线", "TheRender")
	# cv2.namedWindow("TheRender", cv2.WINDOW_KEEPRATIO)
	# while True:
	# 	_image = cv2.cvtColor(cap.capture([403, 225, 228, 140]), cv2.COLOR_RGBA2RGB)
	# 	cv2.imshow("TheRender", _image)
	# 	cv2.waitKey(20)
	
	
	while True:
		capture_make_roi()
		inputed = input("Enter \'Y\' to continue capture, or enter others to EXIT:")
		if inputed != 'Y':
			break
	exit(0)

	# names, ROIs, images, ROI_confirm_dict, ROI_confirm_image_dict = read_data()
	# update_roi_image(names, ROIs, images)
	# exit(0)
	
	
	names, ROIs, images, ROI_confirm_dict, ROI_confirm_image_dict = read_data()
	for name in names:
		print("\"" + name + "\"", ":")
	# ROIs_confirm_dict = dict()
	for name in names:
		try:
			ROI_confirm_dict[name]
		except KeyError:
			ROI_confirm_dict.update({name: dict()})
		key_pressed = show_roi_image(name, ROIs, images)
		if key_pressed == 'y':
			while True:
				print("Input [1] for: adding new rois for this image.")
				print("Input [2] for: update rois for this image.")
				print("Input [3] for: recapture the base image.")
				print("Input [4] for: save confirm image.")
				print("Input [5] for: EXIT edit this image.")
				user_input = input()
				if user_input == '1':
					adding_new_rois(images[name], ROIs[name], name)
				elif user_input == '2':
					update_rois(images[name], ROIs[name], name)
				elif user_input == '3':
					replace_base_image(images[name], ROIs[name], name)
				elif user_input == '4':
					save_confirm_image(images[name], name, ROIs, ROI_confirm_dict)
				elif user_input == '5':
					break
		elif key_pressed == 27:  # Press ESC key.
			break
		json_save(".\\fig\\confirm\\confirm.json", ROI_confirm_dict)
