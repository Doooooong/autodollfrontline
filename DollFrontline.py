import MouseOperation
import pywin32HelperFunc as win32
import numpy as np
import MakeRECT
import opencv
import cv2
import time
import re
import datetime
import threading
from multiprocessing import Lock
import requests
import win32con
import copy


def scale_rect(rect, scale=None):
    LIMIT_X_MAX = 1920
    LIMIT_X_MIN = 0
    LIMIT_Y_MAX = 1080
    LIMIT_Y_MIN = 0
    if rect is None:
        return None
    if scale is None:
        return rect
    rect = copy.deepcopy(rect)  # Do not change the variable that send in.

    center_x = int(rect[0] + rect[2] / 2)
    center_y = int(rect[1] + rect[3] / 2)
    if type(scale) is int:
        w = int(rect[2] * scale)
        h = int(rect[3] * scale)
        add_w = int(rect[2] * scale / 2)
        add_h = int(rect[3] * scale / 2)
    elif type(scale) is list and len(scale) >= 2:
        w = int(rect[2] * scale[0])
        h = int(rect[3] * scale[1])
        add_w = int(rect[2] * scale[0] / 2)
        add_h = int(rect[3] * scale[1] / 2)
    else:
        return rect

    x = center_x - add_w
    y = center_y - add_h
    if center_x - add_w < LIMIT_X_MIN:
        x = 0
    if center_y - add_h < LIMIT_Y_MIN:
        y = 0
    if (x + w) - 1 > LIMIT_X_MAX:
        x = LIMIT_X_MAX - w
    if (y + h) - 1 > LIMIT_Y_MAX:
        y = LIMIT_Y_MAX - h
    return [x, y, w, h]


def apply_rect(img, rect):
    return img[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2]]


def duration_to_second(hours, minutes, seconds):
    return datetime.timedelta(hours=hours, minutes=minutes, seconds=seconds).seconds


class DollFrontlineBase:
    def __init__(self, need_watchdog=True):
        self.doll_name_matching = {
            "GrG11": "GrG[1,i,l,I][1,i,l,I]",
            "FAL": "FAL"
        }
        self.menu_deep = {
            "": -1,
            "0-2放大": 2,
            "8_1夜战放大": 2,
            "编成界面": 1,
            "部署前修复界面": 2,
            "撤退确认界面": 3,
            "到达人形上限": 3,
            "工厂编制扩大界面": 1,
            "工厂分解界面": 1,
            "工厂分解选人界面": 2,
            "工厂强化界面": 1,
            "强化不上升确认界面": -1,
            "人形筛选界面": 2,
            "人形筛选选中界面": 2,
            "探索归来": 1,
            "探索界面": 1,
            "选人界面": 2,
            "远征归来": -1,
            "远征信息界面": 1,
            "远征再度派出": -1,
            "战斗梯队部署界面": 3,
            "战斗选择界面": 1,
            "战斗选择界面_作战_通常": 1,
            "战斗选择界面_作战_紧急": 1,
            "战斗选择界面_作战_夜战": 1,
            "战斗选择界面_作战_活动": 1,
            "战斗选择界面_模拟": 1,
            "战斗选择界面_模拟_心智": 1,
            "战斗选择界面_模拟_资料": 1,
            "战斗选择界面_模拟_资料_准备": 2,
            "战斗选择界面_模拟_资料_出击确认": 3,
            "战斗选择界面_模拟_防御": 1,
            "战斗中": -1,
            "主界面": 0,
            "主界面-无活动": 0,
            "自律再出击界面": -1,
            "作战地图0_2": 2,
            "作战地图8_1_N": 2,
            "有序紊流_1_10_言いたいこと": 2,
            "作战计划设定完成": 2,
            "作战确认界面": 2,
            "作战完成": -1,
            "作战终止界面": 3,
            "闪退后": -1,
            "登录奖励": -1,  # 新的一天更新界面后, 第一个跳出来的.
            "HotNews": -1,  # 登录奖励点完之后,跳出来的
            "Notice": -1,  # HotNews点完之后,跳出来的
            "Event": -1,  # 最后出来的,现有活动的界面
    
            "login": -1,
            "login_faild": -1,
            "twitter_login": -1,
            "before_login": -1,
            # "": -1,

        }
        self.img_names, self.ROIs, self.images, self.ROI_confirm_dict, self.ROI_confirm_image_dict = MakeRECT.read_data()
        self.op = MouseOperation.MouseOperation("少女前线", "TheRender")
        self.cap = win32.Win32_Capture("少女前线", "TheRender")
        self.cap_threading = win32.Win32_Capture("少女前线", "TheRender")
        self.thread_watchdog = threading.Thread(target=self.threadFun_GetState)
        self.state = []
        self.old_watchdog = 0
        self.watchdog = 0
        self.state_mutex = Lock()
        self.SupportReturn = False
        self.ExploreReturn = False
        self.SupportRemainTime = [0, 0, 0, 0, 0, 0]
        self.SupportTimer = [threading.Timer(0, self.support_timer_process),
                             threading.Timer(0, self.support_timer_process),
                             threading.Timer(0, self.support_timer_process),
                             threading.Timer(0, self.support_timer_process),
                             threading.Timer(0, self.support_timer_process),
                             threading.Timer(0, self.support_timer_process)]
        self.ExploreTimer = [threading.Timer(0, self.support_timer_process)]

        self.TimeToSimulation = True
        self.SimulationTimer = [threading.Timer(0, self.simulation_timer_process)]

        self.isZoomOuted = np.zeros((12, 20, 3), dtype=np.bool).tolist()

        self.watchdog_stop = False
        if need_watchdog:
            self.watchdog_stop = True
            self.thread_watchdog.setDaemon(True)
            self.thread_watchdog.start()

        self.AutoExtension = True  # 自动编制扩大
        pass

    def confirm(self, template_scene_name, template_roi_name, name=None, capture_rect=None, capture_rect_scale=1.05,
                threshold=0.9, isGetState=False, watch=False, use_general=False, find_position=False, confirm_res=None):
        rect_org = copy.copy(capture_rect)
        capture_rect = scale_rect(capture_rect, capture_rect_scale)
        rect = copy.copy(capture_rect)
        # if cap == "Threading":
        # 	__cap = self.cap_threading
        # else:
        __cap = self.cap
        if name is None:  # The name of extra confirm image is not set.
            # Template is from scene image with roi of [roi_name].
            rect_org = self.ROIs[template_scene_name][template_roi_name]
            img_template = apply_rect(self.images[template_scene_name], rect_org)

            # Compare image is from capture.
            if capture_rect is None:  # Capture ROI is not set. Use roi of [roi_name].
                rect_org = copy.copy(self.ROIs[template_scene_name][template_roi_name])
                rect = scale_rect(copy.copy(rect_org), capture_rect_scale)
                input_image = cv2.cvtColor(__cap.capture(rect), cv2.COLOR_RGBA2RGB)
            else:  # Capture ROI is set.
                input_image = cv2.cvtColor(__cap.capture(capture_rect), cv2.COLOR_RGBA2RGB)
            pass
        else:  # The name of extra confirm image is set.
            try:  # Confirm the name of extra is really exist.
                if use_general:
                    num_candident = self.ROI_confirm_dict[template_scene_name]["General"].__len__()
                else:
                    num_candident = self.ROI_confirm_dict[template_scene_name][template_roi_name].__len__()
                if num_candident == 0:  # The name of extra list is empty, raise error.
                    raise ValueError
                else:  # The name of extra list is not empty
                    if use_general:
                        img_template = self.ROI_confirm_image_dict[template_scene_name]["General"][name]
                    else:
                        img_template = self.ROI_confirm_image_dict[template_scene_name][template_roi_name][name]
                    if capture_rect is None:
                        rect_org = copy.copy(self.ROIs[template_scene_name][template_roi_name])
                        rect = scale_rect(copy.copy(rect_org), capture_rect_scale)
                        input_image = cv2.cvtColor(__cap.capture(rect), cv2.COLOR_RGBA2RGB)
                    else:
                        input_image = cv2.cvtColor(__cap.capture(capture_rect), cv2.COLOR_RGBA2RGB)

            except (KeyError, AttributeError):  # Confirm the name of extra is not  exist.
                rect_org = self.ROIs[template_scene_name][template_roi_name]
                img_template = apply_rect(self.images[template_scene_name], rect_org)
                rect = scale_rect(rect_org, capture_rect_scale)
                if capture_rect is None:
                    input_image = cv2.cvtColor(__cap.capture(rect), cv2.COLOR_RGBA2RGB)
                else:
                    input_image = cv2.cvtColor(__cap.capture(capture_rect), cv2.COLOR_RGBA2RGB)
            pass

        if watch:
            # print(template_scene_name)
            MakeRECT.cv2_imshow("input_image", input_image)
            MakeRECT.cv2_imshow("img_template", img_template)
        pass
        # timestr = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S.%f_')
        # _img_file_name = template_scene_name + template_roi_name + str(name) + timestr
        # cv2.imencode('.png', input_image)[1].tofile(".\\log\\" + _img_file_name + ".png")
        # cv2.imwrite("./log/", template_scene_name + template_roi_name + str(name) + timestr, input_image)
        confirm_result = opencv.compare_image(input_image, img_template, threshold)
        
        if confirm_res is None:
            confirm_res = [0]
        confirm_res[0] = confirm_result  # opencv.compare_hsv_image(input_image, img_template, threshold)
        
        if find_position:
            pos = opencv.find_image(input_image, img_template, threshold)
            pos[0] += (rect[0] - rect_org[0])
            pos[1] += (rect[1] - rect_org[1])
        if confirm_result < threshold:
            if isGetState == False:
                try:
                    print(datetime.datetime.now(),
                          " [" + template_scene_name + "] ", "->",
                          " [" + template_roi_name + "]", "->",
                          " [" + name + "]", "->",
                          " confirm failed!", "score: ", confirm_result)
                except TypeError:
                    print(datetime.datetime.now(),
                          " [" + template_scene_name + "] ", "->",
                          " [" + template_roi_name + "]",
                          " confirm failed!", "score: ", confirm_result)
            if find_position:
                return False, pos
            return False
        else:
            if find_position:
                return True, pos
            return True

    def send_msg_to_phone(self):
        url = "https://maker.ifttt.com/trigger/DollsFrontlineNotify/with/key/hO4O9_IK8DEHRPjmJYZ-37u-J_RonYgmlmh_8ZSvP4Y"
        response = requests.request("POST", url)

    def send_fault_and_stop_program(self):
        self.send_msg_to_phone()
        while True:
            time.sleep(180)

    def threadFun_GetState(self):
        self.old_watchdog = self.watchdog
        self.stop_timeout = 0
        while True:
            time.sleep(20)
            if self.watchdog_stop == True:
                continue
            if self.confirm("闪退后", "state_confirm_1") == True:
                self.process_StartUp()
                isAppExit = True
                continue
            # if self.watchdog == self.old_watchdog:
            self.stop_timeout += 1
            if self.stop_timeout > 30:  # Over 600s.
                self.send_msg_to_phone()
                while True:
                    time.sleep(180)

    def getState(self, state=None, threshold=0.9):
        self.state = []

        if state is not None:
            try:
                ____ = self.ROIs[state]['state_confirm_1']
                if self.confirm(state, 'state_confirm_1', threshold=threshold, isGetState=True):
                    self.state.append(state)
                    return self.state
                self.state = ['']
                return ['']
            except KeyError:
                self.state = ['']
                return [""]

        for scene_name in self.ROIs:
            try:
                ____ = self.ROIs[scene_name]['state_confirm_1']
                if self.confirm(scene_name, 'state_confirm_1', threshold=threshold, isGetState=True):
                    self.state.append(scene_name)
            except KeyError:
                pass
        if self.state.__len__() > 1:
            for scene_name in self.state:
                try:
                    ____ = self.ROIs[scene_name]['state_confirm_2']
                    ____ = self.ROIs[scene_name]['btn_Confirm']
                    ____ = self.ROIs[scene_name]['btn_Return']
                    if self.confirm(scene_name, 'state_confirm_2', threshold=threshold,
                                    isGetState=True):
                        # self.state.append(scene_name)
                        pass
                    elif self.confirm(scene_name, 'btn_Confirm', threshold=threshold,
                                      isGetState=True):
                        pass
                    elif self.confirm(scene_name, 'btn_Return', threshold=threshold,
                                      isGetState=True):
                        pass
                    else:
                        __idx = self.state.index(scene_name)
                        self.state.pop(__idx)
                except KeyError:
                    pass
        if self.state.__len__() == 0:
            self.state = ['']
        print(datetime.datetime.now(), "getState() result is: ", self.state)
        return self.state

    def read_state(self):
        while not self.state_mutex.acquire():
            pass
        ret = self.state
        self.state_mutex.release()
        return

    def get_appeared_chapter(self, range_low=0, range_high=10):
        Upper_chapter = 0
        Upper_chapter_pos = [0, 0]
        # Lower_chapter = 9
        # Lower_chapter_pos = [0, 0]
        Selected_chapter = -1
        Selected_chapter_pos = [0, 0]
        for i in range(range_low, range_high):
            upper_res1, upper_pos1 = self.confirm("战斗选择界面_作战_通常", "text_chapter_select",
                                                  "btn_Chapter" + str(i) + "_Unselected",
                                                  capture_rect_scale=1.2, find_position=True, threshold=0.97)
            if upper_res1 == False:
                upper_res2, upper_pos2 = self.confirm("战斗选择界面_作战_通常", "text_chapter_select",
                                                      "btn_Chapter" + str(i) + "_Selected",
                                                      capture_rect_scale=1.2, find_position=True, threshold=0.97)
                if upper_res2 == False:
                    continue
                else:
                    Selected_chapter = i
                    Selected_chapter_pos = upper_pos2
                    Upper_chapter = i
                    Upper_chapter_pos = upper_pos2
                    break
            else:
                Upper_chapter = i
                Upper_chapter_pos = upper_pos1
                break
        return [Upper_chapter, Upper_chapter_pos, Selected_chapter, Selected_chapter_pos]

    def select_battle_chapter(self, chp_num=0):
        __st_rect = self.ROIs["战斗选择界面_作战_通常"]["btn_Chapter_Slot1"]
        __ed_rect = self.ROIs["战斗选择界面_作战_通常"]["btn_Chapter_Slot5"]
        while True:
            ret = self.get_appeared_chapter()
            print("=======")
            if ret[0] <= chp_num <= ret[0] + 4:
                upper_res, __chp_pos = self.confirm("战斗选择界面_作战_通常", "text_chapter_select",
                                                    "btn_Chapter" + str(chp_num) + "_Unselected",
                                                    capture_rect_scale=1.2, find_position=True, threshold=0.97)

                if upper_res == False:
                    upper_res, __chp_pos = self.confirm("战斗选择界面_作战_通常", "text_chapter_select",
                                                        "btn_Chapter" + str(chp_num) + "_Selected",
                                                        capture_rect_scale=1.2, find_position=True, threshold=0.97)

                baseline_st = self.ROIs["战斗选择界面_作战_通常"]["text_chapter_select"]
                baseline_size = self.ROIs["战斗选择界面_作战_通常"]["btn_Chapter_Slot1"]
                chp_pos = [baseline_st[0] + __chp_pos[0], baseline_st[1] + __chp_pos[1], baseline_size[2],
                           baseline_size[3]]
                break
            elif chp_num < ret[0]:
                self.op.drag_rect(__st_rect, __ed_rect)
                self.op.rnd_sleep(3000)
            elif chp_num > ret[0] + 4:
                self.op.drag_rect(__ed_rect, __st_rect)
                self.op.rnd_sleep(3000)
        self.op.click_in_rect(chp_pos, [1000, 200])
        return True

    def old_select_battle_chapter(self, chp_num=0):
        scene_name = "战斗选择界面_作战_通常"
        slot_name = "btn_Chapter_Slot" + str(chp_num % 5 + 1)
        if not self.confirm(scene_name, "state_confirm_1", capture_rect_scale=1.2):
            return False
        __timeout_1 = 10
        self.op.rnd_sleep(1500)
        while not self.confirm(scene_name, slot_name, "btn_Chapter0_Selected", capture_rect_scale=1.2):
            __timeout_2 = 10
            while (not self.confirm(scene_name, slot_name, "btn_Chapter0_Unselected", capture_rect_scale=1.2)) and (
                    not self.confirm(scene_name, slot_name, "btn_Chapter0_Selected", capture_rect_scale=1.2)):
                # Drag
                __st_rect = self.ROIs[scene_name]["btn_Chapter_Slot1"]
                __ed_rect = self.ROIs[scene_name]["btn_Chapter_Slot6"]
                self.op.drag_rect(__st_rect, __ed_rect)
                self.op.rnd_sleep(3000)
                __timeout_2 -= 1
                if __timeout_2 <= 0:
                    return False
            # Click chp btn.
            __rect = self.ROIs[scene_name][slot_name]
            self.op.click_in_rect(__rect)
            self.op.rnd_sleep(1000)
            __timeout_1 -= 1
            if __timeout_1 <= 0:
                return False
        return True

    def select_battle_section(self, chp_num=0, sec_num=1, mode=""):
        sec_name = "btn_Section_Slot" + str(sec_num % 4)
        confirm_img_name = str(chp_num) + "_" + str(sec_num) + "_" + mode
        self.click_ex("战斗选择界面_作战_通常", sec_name,
                      pre_confirm=["战斗选择界面_作战_通常", "text_Section_Slot" + str(sec_num % 4), confirm_img_name, True],
                      after_confirm=["作战确认界面", "state_confirm_1", None, True], stuck_mode=[False, True])

        return True

    def return_process(self, scene_name):
        for name in self.getState():
            # __rect = self.ROIs[name]['btn_Return']
            # self.op.click_in_rect(__rect, [1000, 300])
            try:
                __rect = self.ROIs[name]['btn_Return']
                self.op.click_in_rect(__rect, [1000, 300])
                return True
            except KeyError:
                continue
                # self.op.send_backspace_key()
        return False

    def back_to_base(self):
        scene_name = "主界面"
        confirm_name = "state_confirm_1"

        __timeout = 25
        while self.getState() == ['']:
            self.op.rnd_sleep(100, 30)
        while self.menu_deep[self.state[0]] != 0:
            if self.menu_deep[self.state[0]] != -1:
                self.return_process(self.state[0])
            if self.state[0] == "远征再度派出" or self.state[0] == "远征归来":
                self.处理远征()
                pass
            if self.state[0] == '闪退后':
                self.process_StartUp2()

            self.op.rnd_sleep(500, 30)
            while self.getState() == ['']:
                self.op.rnd_sleep(100, 30)
            __timeout -= 1
            if __timeout <= 0:
                return False
        if "远征信息界面" in self.state:
            self.click_ex("远征信息界面", "btn_远征信息", after_confirm=["主界面", "btn_远征信息", None, True])

    def skip_animation(self, timeout=50):
        _count = timeout
        while self.getState() == ['']:
            self.op.rnd_sleep(100, 30)
            _count -= 1
            if _count <= 0:
                return False
        return True

    def 处理远征(self):
        while True:
            self.skip_animation()

            scene_name = "远征归来"
            confirm1_name = "state_confirm_1"
            远征1_ret1 = self.confirm(scene_name, confirm1_name)
            # 远征1_ret2 = self.confirm(scene_name, confirm2_name)

            scene2_name = "远征再度派出"
            confirm1_name = "state_confirm_1"
            btn_name = "btn_confirm"
            远征2_ret1 = self.confirm(scene2_name, confirm1_name)
            # 远征2_ret2 = self.confirm(scene_name, confirm1_name)

            if 远征1_ret1 or 远征2_ret1:
                __rect = self.ROIs[scene2_name][btn_name]
                self.op.click_in_rect(__rect)
                self.op.rnd_sleep(400, 50)

                __timeout = 25
                while self.confirm(scene2_name, confirm1_name):
                    __rect = self.ROIs[scene2_name][btn_name]
                    self.op.click_in_rect(__rect)
                    self.op.rnd_sleep(400, 50)
                    __timeout -= 1
                    if __timeout <= 0:
                        return False
            else:
                return
            self.op.rnd_sleep(4000, 200)

    def click_Battle(self):
        if self.getState()[0] == '主界面-无活动':
            self.click_ex("主界面-无活动", "btn_战斗", after_confirm=["战斗选择界面_作战_通常", "state_confirm_1", None, True], delay=[2500, 300] )
        else:
            self.click_ex("主界面", "btn_战斗", after_confirm=["战斗选择界面_作战_通常", "state_confirm_1", None, True], delay=[2500, 300])

    # scene_name = "主界面"
    # confirm1_name = "state_confirm_1"
    # if self.confirm(scene_name, confirm1_name):
    # 	__rect = self.ROIs[scene_name]["btn_战斗"]
    # 	self.op.click_in_rect(__rect, [1000, 10])

    def click_分解(self):
        scene_name = "到达人形上限"
        confirm1_name = "state_confirm_1"
        if self.confirm(scene_name, confirm1_name):
            __rect = self.ROIs[scene_name]['btn_前往强化']
            self.op.click_in_rect(__rect, [3000, 200])
            return True
        else:
            return False

    def click(self, scene, button, delay=None, confirm="state_confirm_1", noCheck=False):
        if delay is None:
            delay = [800, 300]

        scene_name = scene
        confirm1_name = confirm
        try:
            ____ = self.ROIs[scene_name][confirm1_name]
        except KeyError:
            confirm1_name = button
        if noCheck or self.confirm(scene_name, confirm1_name):
            __rect = self.ROIs[scene_name][button]
            print("Click [", scene_name, "] -> [", button, "]")
            self.op.click_in_rect(__rect, delay)
            return True
        else:
            print("No Click [", scene_name, "] -> [", button, "]")
            return False

    def click_ex(self, scene, btn, delay=None,
                 pre_confirm=None, after_confirm=None,
                 stuck_mode=None, timeout=4, loop_duty=2000, click_cnt=1000, click_roi=None):
        '''
        
        :param scene:
        :param btn:
        :param delay: 2-length list, first number is delay before the click, and the second number is delay after the click.
        :param pre_confirm: 4-length list, 1-3 items are the parameters for confirm() function, means to locate the image for confirming
                                1st item is the [scene name]: ""
                                2nd item is the [roi name]: ""
                                3rd item is the [ex name]: DEFAULT:None; Normally, 1st and 2nd item can locate the image,
                                                            but the region [roi name] maybe has other alternative images to confirm.
                                                            This [ex name] should in the dictionary of self.
                                4th item is the [condition polarity]: DEFAULT is True; Means if the confirm result is True, the program is go ahead; And will stuck while confirm result is False.
                                                                        If you set [condition polarity] to False, it means if the confirm result is True, the program is [STUCKED]; And will [GO AHEAD] if confirm result is False.
                                
        :param after_confirm:
        :param stuck_mode:
        :param timeout:
        :param loop_duty:
        :param click_cnt:
        :return:
        '''
        _timeout_cnt = 0
        noCheck = [False, False]
        if pre_confirm is None:
            noCheck[0] = True
        if after_confirm is None:
            noCheck[1] = True
        if delay is None:
            delay = [1000, 300]
        if stuck_mode is None:
            stuck_mode = [False, False]
        if click_roi == None:
            click_roi = self.ROIs[scene][btn]

        if noCheck[0] == False:
            try:
                self.ROIs[pre_confirm[0]]
                pre_scene = pre_confirm[0]
            except KeyError:
                pre_scene = scene

            try:
                self.ROIs[pre_confirm[0]][pre_confirm[1]]
                pre_roi = pre_confirm[1]
            except KeyError:
                pre_roi = btn

            if stuck_mode[0] == True:
                while self.confirm(pre_scene, pre_roi, pre_confirm[2]) == (not pre_confirm[3]):
                    _timeout_cnt += 1
                    time.sleep(loop_duty / 1000)
                    if _timeout_cnt > timeout:
                        return False
            else:
                if self.confirm(pre_scene, pre_roi, pre_confirm[2]) == (not pre_confirm[3]):
                    return False

        if noCheck[1] == True:
            print("Click [", scene, "] -> [", btn, "]")
            self.op.click_in_rect(click_roi, delay)
        else:
            try:
                _ = self.ROIs[after_confirm[0]]
                after_scene = after_confirm[0]
            except KeyError:
                after_scene = scene

            try:
                _ = self.ROIs[after_confirm[0]][after_confirm[1]]
                after_roi = after_confirm[1]
            except KeyError:
                after_roi = btn

            if stuck_mode[1] == True:
                while self.confirm(after_scene, after_roi, after_confirm[2]) == (not after_confirm[3]):
                    if click_cnt > 0:
                        print("Click [", scene, "] -> [", btn, "]")
                        self.op.click_in_rect(click_roi, delay)
                        click_cnt -= 1
                    _timeout_cnt += 1
                    time.sleep(loop_duty / 1000)
                    if _timeout_cnt > timeout:
                        return False
            else:
                print("Click [", scene, "] -> [", btn, "]")
                self.op.click_in_rect(click_roi, delay)
                if self.confirm(after_scene, after_roi, after_confirm[2]) == (not after_confirm[3]):
                    return False
        return True

    def process_分解(self):
        self.click_ex("工厂分解界面", "btn_分解功能", after_confirm=["工厂分解界面", "state_confirm_2", None, True])
        self.click_ex("工厂分解界面", "btn_选人", after_confirm=["工厂分解选人界面", "state_confirm_1", None, True])
        self.click_ex("工厂分解选人界面", "btn_AutoSelect", after_confirm=["工厂分解界面", "state_confirm_2", None, True],
                      stuck_mode=[False, True])
        self.click_ex("工厂分解界面", "btn_Confirm", delay=[800, 100])

    # scene_name = "工厂分解界面"
    # confirm1_name = "state_confirm_1"
    # self.op.rnd_sleep(2000, 500)
    # if self.confirm(scene_name, confirm1_name):
    # 	__rect = self.ROIs[scene_name]['btn_分解功能']
    # 	self.op.click_in_rect(__rect, [1200, 10])
    # 	confirm1_name = "state_confirm_2"
    # 	if self.confirm(scene_name, confirm1_name):
    # 		__rect = self.ROIs[scene_name]['btn_选人']
    # 		self.op.click_in_rect(__rect, [1200, 10])
    # 		scene_name = "工厂分解选人界面"
    # 		confirm1_name = "state_confirm_1"
    # 		if self.confirm(scene_name, confirm1_name):
    # 			__rect = self.ROIs[scene_name]['btn_AutoSelect']
    # 			self.op.click_in_rect(__rect, [400, 10])
    # 			__rect = self.ROIs[scene_name]['btn_AutoSelect']
    # 			self.op.click_in_rect(__rect, [400, 10])
    # 			__rect = self.ROIs["工厂分解界面"]['btn_Confirm']
    # 			self.op.click_in_rect(__rect, [400, 10])
    # self.back_to_base()

    def process_工厂(self):
        # if self.confirm("工厂编制扩大界面", "confirm_可以编制扩大"):
        self.process_扩大()
        ret = self.process_强化()
        if ret == "无可强化人形":
            self.process_分解()
            pass
        self.back_to_base()

    def process_强化(self):
        if self.getState("工厂强化界面") != ["工厂强化界面"]:
            if not self.click_ex("工厂强化界面", "btn_强化功能", after_confirm=["工厂强化界面", "state_confirm_1", None, True]):
                return False
        while True:
            if not self.click_ex("工厂强化界面", "btn_强化人形选择", after_confirm=["工厂分解选人界面", "btn_Return", None, True]):
                return "无可强化人形"

            # 选中首页可以被强化的人形
            end_flag = False
            for row in [1, 2]:
                for col in [1, 2, 3, 4, 5, 6]:
                    if not self.click_ex("选人界面", "btn_Select_" + str(row) + "-" + str(col),
                                         after_confirm=["工厂强化界面", "state_confirm_1", None, True], timeout=2):
                        continue
                    end_flag = True
                    break
                if end_flag == True:
                    break
            if end_flag == False:
                self.click_ex("选人界面", "btn_Return", delay=[300, 100],
                              after_confirm=["工厂强化界面", "state_confirm_1", None, True])
                return "无可强化人形"

            while True:
                # 进入选择狗粮界面
                if not self.click_ex("工厂强化界面", "btn_狗粮选择", after_confirm=["工厂分解选人界面", "btn_Return", None, True]):
                    return False
                # 自动选择狗粮
                hasValiable = self.confirm("工厂分解选人界面", "confirm_2Star_doll")
                # # 狗粮存在
                if hasValiable == True:
                    # 点完【自动选择】后，该按钮会变成【确认】
                    self.click_ex("工厂分解选人界面", "btn_AutoSelect")
                    self.op.rnd_sleep(500, 100)
                    if not self.click_ex("工厂分解选人界面", "btn_AutoSelect",
                                         after_confirm=["工厂强化界面", "state_confirm_1", None, True],
                                         timeout=4, loop_duty=500):
                        # 狗粮存在，却无法自动选择，代表人形状态已满。
                        # 返回至工厂强化界面
                        if not self.click_ex("工厂分解选人界面", "btn_Return",
                                             after_confirm=["工厂强化界面", "state_confirm_1", None, True]):
                            return True
                        # 返回并重新选择角色
                        print("人形状态已满，返回并重新选择角色")
                        break
                    if not self.click_ex("工厂强化界面", "btn_强化", after_confirm=["强化不上升确认界面", "btn_confirm", None, True]):
                        # 狗粮充足，直接强化
                        self.op.rnd_sleep(700, 50)
                        for i in range(10):
                            # 连续点击，防止出现【强化完成】界面
                            self.click_ex("工厂强化界面", "btn_强化", delay=[300, 100])
                        continue
                    else:
                        # 狗粮不足，需要点确认。
                        if not self.click_ex("强化不上升确认界面", "btn_confirm",
                                             after_confirm=["工厂强化界面", "state_confirm_1", None, True]):
                            return False
                        self.op.rnd_sleep(700, 50)
                        for i in range(10):
                            # 连续点击，防止出现【强化完成】界面
                            self.click_ex("工厂强化界面", "btn_强化", delay=[300, 100])
                        # 狗粮不足，可直接退出强化。
                        print("狗粮不足，返回至工厂强化界面")
                        return True
                    pass
                # # 狗粮不存在
                else:
                    # 返回至工厂强化界面
                    print("狗粮不存在，返回至工厂强化界面")
                    if not self.click_ex("工厂分解选人界面", "btn_Return",
                                         after_confirm=["工厂强化界面", "state_confirm_1", None, True]):
                        return False
                    return True

    def process_扩大(self):
        if self.AutoExtension == False:
            return "无可扩大人形"
        if not self.click_ex("工厂编制扩大界面", "btn_编制扩大功能", after_confirm=["工厂编制扩大界面", "state_confirm_1", None, True]):
            return False
        while self.click_ex("工厂编制扩大界面", "btn_编制扩大角色选择",
                            after_confirm=["工厂分解选人界面", "btn_Return", None, True],
                            timeout=2):
            if not self.click_ex("选人界面", "btn_Select_1-1", after_confirm=["工厂编制扩大界面", "state_confirm_1", None, True]):
                return ""
            if not self.click_ex("工厂编制扩大界面", "btn_confirm", after_confirm=["强化不上升确认界面", "btn_confirm", None, True]):
                return ""
            if not self.click_ex("强化不上升确认界面", "btn_confirm", after_confirm=["工厂编制扩大界面", "state_confirm_1", None, True]):
                return ""
        return '无可扩大人形'

    def process_StartUp(self, timeout=30):
        self.op.rnd_sleep(3000, 3000)
        self.click_ex("闪退后", "state_confirm_1", after_confirm=["闪退后", "state_confirm_1", None, False])
        self.op.rnd_sleep(13000, 3000)
        print("sleep finished")
        _timeout = 0
        while self.getState("before_login") == ['before_login']:
            self.click_ex("主界面", "btn_startup", delay=[5000, 4000],
                          pre_confirm=["login", "btn_login", None, False])
            _timeout += 1
            if _timeout > timeout:
                return False
        # update
        print("update")
        if self.getState("更新界面") == ["更新界面"]:
            self.click_ex("更新界面", "btn_confirm")
            _timeout = 0
            while self.getState() == ['']:
                self.click_ex("主界面", "btn_startup", delay=[5000, 4000],
                                      pre_confirm=["before_login", "state_confirm_1", None, True])
                _timeout += 1
                if _timeout > timeout:
                    return False
            return False

        # login
        time.sleep(1)
        print("login")
        if self.getState("login") == ["login"]:
            self.click_ex("login", "btn_login")
            _timeout = 0
            while self.getState() == ['']:
                time.sleep(2)
                _timeout += 1
                if _timeout > timeout:
                    return False
            # # twitter_login
            self.click_ex("twitter_login", "btn_login", delay=[6000, 100],
                          pre_confirm=["twitter_login", "state_confirm_1", None, True],
                          after_confirm=["twitter_login", "state_confirm_1", None, False])
            print("twitter_login", "::btn_login", "  sleep finished!")
            _timeout = 0
            while self.getState() == [''] or self.getState("login") == ['login']:
                time.sleep(1)
                _timeout += 1
                if _timeout > timeout:
                    return False
    
            # login_failed
            if self.getState("login_faild") == ["login_faild"]:
                return False
    
            _timeout = 0
            while self.getState() == ['']:
                self.op.click_in_rect(self.ROIs["主界面"]["btn_startup"], [5000, 4000])
                _timeout += 1
                if _timeout > timeout:
                    return False
        
        
        self.isZoomOuted[0][2][0] = False
        return True

    def process_StartUp2(self, timeout=30):
        self.op.rnd_sleep(3000, 3000)
        self.click_ex("闪退后", "state_confirm_1", after_confirm=["闪退后", "state_confirm_1", None, False])
        self.op.rnd_sleep(13000, 3000)
        print("sleep finished")
        _timeout = 0
        while True:
            time.sleep(2)
            if self.getState("login") == ["login"]:
                print("login sequence.")
                self.click_ex("login", "btn_login")
                _timeout = 0
                while self.getState() == ['']:
                    time.sleep(2)
                    _timeout += 1
                    if _timeout > timeout:
                        return False
                # # twitter_login
                self.click_ex("twitter_login", "btn_login", delay=[6000, 100],
                              pre_confirm=["twitter_login", "state_confirm_1", None, True],
                              after_confirm=["twitter_login", "state_confirm_1", None, False])
                print("twitter_login", "::btn_login", "  sleep finished!")
                _timeout = 0
                while self.getState() == [''] or self.getState("login") == ['login']:
                    time.sleep(1)
                    _timeout += 1
                    if _timeout > timeout:
                        return False
        
                # login_failed
                if self.getState("login_faild") == ["login_faild"]:
                    return False
        
                _timeout = 0
                while self.getState() == ['']:
                    self.op.click_in_rect(self.ROIs["主界面"]["btn_startup"], [5000, 4000])
                    _timeout += 1
                    if _timeout > timeout:
                        return False
            elif self.getState("before_login") == ['before_login']:
                self.click_ex("主界面", "btn_startup", delay=[7000, 400],
                              pre_confirm=["login", "btn_login", None, False])
                _timeout += 1
                if _timeout > timeout:
                    return False
            # update
            elif self.getState("更新界面") == ["更新界面"]:
                print("update")
                self.click_ex("更新界面", "btn_confirm")
                _timeout = 0
                while self.getState() == ['']:
                    self.click_ex("主界面", "btn_startup", delay=[5000, 4000],
                                  pre_confirm=["before_login", "state_confirm_1", None, True])
                return False
            elif self.getState("主界面-无活动") == ["主界面-无活动"] or self.getState("主界面") == ["主界面"]:
                break
            else:
                self.处理远征()
                continue
            


    
        self.isZoomOuted[0][2][0] = False
        return True
    
    def find_rect_in_big_rect(self, scene_name, search_range_rect, finding_rect, confirm_img_name):
        """
        Usage:
            self.find_rect_in_big_rect(scene_name="战斗选择界面",
                           search_range_rect="text_battle_selection",
                           finding_rect="btn_Chapter_Slot1",
                           confirm_img_name="btn_battle_Unselected")
        :param scene_name: 
        :param search_range_rect: Big range you want to search for.
        :param finding_rect: Final size of rect you want to get. 
        :param confirm_img_name: The content that you want to search in the [search_range_rect].
        :return: A rect with the size of [finding_rect].
        """
        confirm_res, __chp_pos = self.confirm(scene_name, search_range_rect,
                                            confirm_img_name,
                                            capture_rect_scale=1.2, find_position=True, threshold=0.97, use_general=True)
        if confirm_res is True:
            baseline_st = self.ROIs[scene_name][search_range_rect]
            baseline_size = self.ROIs[scene_name][finding_rect]
            return [baseline_st[0] + __chp_pos[0], baseline_st[1] + __chp_pos[1], baseline_size[2],
                       baseline_size[3]]
        else:
            return False
        
    def select_battle_type(self, battle_type: str):
        if self.getState("战斗选择界面")[0] != "战斗选择界面":
            return False
        if battle_type not in ["battle", "campaign", "simulate", "support"]:
            return False
        while True:
            ret = self.find_rect_in_big_rect(scene_name="战斗选择界面",
                                       search_range_rect="text_battle_selection",
                                       finding_rect="btn_battle_slot1",
                                       confirm_img_name="btn_" + battle_type + "_Selected")
            if ret == False:
                ret = self.find_rect_in_big_rect(scene_name="战斗选择界面",
                                                 search_range_rect="text_battle_selection",
                                                 finding_rect="btn_battle_slot1",
                                                 confirm_img_name="btn_" + battle_type + "_Unselected")
                self.click_ex("战斗选择界面", "", click_roi=ret, delay=[1800, 200])
            else:
                return
    
    def waiting_while(self, scene_name, confirm_rect_name, check_period: int = 1):
        while not self.confirm(scene_name, confirm_rect_name):
            time.sleep(check_period)
    
    # while self.getState("战斗选择界面_作战_通常")[0] != "战斗选择界面_作战_通常" and self.getState("战斗选择界面_作战_活动")[0] != "战斗选择界面_作战_活动":
    #     # if self.state[0] == '远征归来':
    #     #     return False
    #     if self.getState("战斗选择界面_作战_活动")[0] == "战斗选择界面_作战_活动":
    #         self.click("战斗选择界面_作战_活动", "btn_作战", [1500, 0], confirm="state_confirm_2")
    #     elif self.getState("战斗选择界面_作战_通常")[0] == "战斗选择界面_作战_通常":
    #         self.click("战斗选择界面_作战_通常", "btn_作战", [1500, 0], confirm="state_confirm_2")
    #     elif self.getState("战斗选择界面")[0] == "战斗选择界面":
    #         self.click("战斗选择界面_作战_通常", "btn_作战", [1500, 0], confirm="state_confirm_2")
    #     elif self.getState("主界面-无活动")[0] == "主界面-无活动" or self.getState("主界面")[0] == "主界面":
    #         self.click_Battle()
    #     else:
    #         self.处理远征()
    # upper_res, __chp_pos = self.confirm("战斗选择界面", "text_chapter_select",
    #                                     "btn_Chapter" + str(chp_num) + "_Unselected",
    #                                     capture_rect_scale=1.2, find_position=True, threshold=0.97)
    def select_enter_map(self, chp_num, sec_num, mode=""):
        while self.select_battle_type("battle") == False:
            if self.getState("主界面-无活动")[0] == "主界面-无活动" or self.getState("主界面")[0] == "主界面":
                self.click_Battle()
            time.sleep(2)
            self.处理远征()
        
        if not self.select_battle_chapter(chp_num):
            return False

        self.op.rnd_sleep(1300, 300)
        if mode == "":
            if self.getState("战斗选择界面_作战_通常") != "战斗选择界面_作战_通常":
                self.click("战斗选择界面_作战_通常", "btn_通常", [800, 0])
        elif mode == "E":
            if self.getState("战斗选择界面_作战_紧急") != "战斗选择界面_作战_紧急":
                self.click("战斗选择界面_作战_通常", "btn_紧急", [800, 0])
        elif mode == "N":
            if self.getState("战斗选择界面_作战_夜战") != "战斗选择界面_作战_夜战":
                self.click("战斗选择界面_作战_通常", "btn_夜战", [800, 0])

        if not self.select_battle_section(chp_num, sec_num, mode):
            return False

        self.click("作战确认界面", "btn_作战开始", [800, 0])
        # scene_name = "作战确认界面"
        # confirm1_name = "state_confirm_1"
        # self.op.rnd_sleep(1200, 500)
        #
        # __rect = self.ROIs[scene_name]['btn_作战开始']
        # while self.getState("作战确认界面", threshold=0.97) == ["作战确认界面"]:
        #     self.op.click_in_rect(__rect, [800, 10])
        # self.op.rnd_sleep(2500, 500)
        return True

    def change_doll(self, slot: int=1, doll_name: str = "", filter_GUN: list = [], filter_rare: list = []):
        '''

        :param slot:
        :param doll_name:
        :param filter_GUN: String in ["HG", "SMG", "RF", "AR", "MG", "SG", "MAX_LV", "NoMAX_LV"]
        :param filter_rare: String in ["5", "4", "3", "2", "1", "Ex", "6"]
        :return:
        '''
        if doll_name == "NoChange":
            return
        scene_name = "编成界面"
        confirm1_name = "state_confirm_1"
        click_name = "btn_Select_Slot" + str(slot)
        self.op.rnd_sleep(1000, 500)
        while not self.confirm("编成界面", "text_Name_Slot" + str(slot), "Gun_" + doll_name, use_general=True,
                             capture_rect_scale=1.5):

            if self.confirm(scene_name, confirm1_name, capture_rect_scale=1.2):
                __rect = self.ROIs[scene_name][click_name]
                # self.op.click_in_rect(__rect, [400, 50])
                self.click_ex(scene_name, click_name, after_confirm=["选人界面", "state_confirm_1", None, True],
                              stuck_mode=[False, True], delay=[800, 200])
    
                if len(filter_GUN) != 0 or len(filter_rare) != 0:
                    self.click_ex("选人界面", "btn_枪种", after_confirm=["人形筛选界面", "state_confirm_1", None, True],
                                  stuck_mode=[False, True], delay=[400, 80])
                    # self.click_ex("人形筛选界面", "btn_Confirm", delay=[400, 80])
    
                    for gun_type in filter_GUN:
                        self.click_ex("人形筛选界面", "btn_" + gun_type, after_confirm=["人形筛选选中界面", "btn_" + gun_type, None, True],
                                      stuck_mode=[False, True], delay=[200, 80])
                    for rare in filter_rare:
                        self.click_ex("人形筛选界面", "btn_Star" + rare, after_confirm=["人形筛选选中界面", "btn_Star" + rare, None, True],
                                      stuck_mode=[False, True], delay=[200, 80])
    
                    self.click_ex("人形筛选界面", "btn_Confirm", after_confirm=["人形筛选选中界面", "state_confirm_1", None, False],
                                  stuck_mode=[False, True], delay=[400, 80])
    
                scene_name = "选人界面"
                for slot_name in self.ROIs[scene_name]:
                    if slot_name.find("text_Name") != -1:
                        if self.confirm(scene_name, slot_name, doll_name, use_general=True, capture_rect_scale=[1.1, 1.5]):
                            print("Find in: ", slot_name, ". Found : ", doll_name)
                            btn_name = "btn_Select" + slot_name.split("text_Name")[1]
                            self.click_ex("选人界面", btn_name, after_confirm=["编成界面", "state_confirm_2", None, True],
                                          stuck_mode=[False, True], delay=[1000, 80])
                            # self.click_ex(scene_name, "btn_Select" + slot_name.split("text_Name")[1])
                            return
                
                
                
                # print("Find in: ", slot_name, ". Not found: ", doll_name)
                # __rect = self.ROIs[scene_name][slot_name]
                # _image = cv2.cvtColor(self.cap.capture(__rect), cv2.COLOR_RGBA2RGB)
                # name_text = opencv.text_ocr(_image, "").replace(' ', '').replace('\n', '')
                # match_res = re.findall(self.doll_name_matching[doll_name], name_text)
                # if match_res != []:
                # 	print("Find in: ", slot_name, ". Found : ", doll_name)
                # 	__rect = self.ROIs[scene_name]["btn_Select" + slot_name.split("text_Name")[1]]
                # 	self.op.click_in_rect(__rect, [1400, 50])
                # 	return
                # print("Find in: ", slot_name, ". OCR result: ", name_text, ". Not found: ", doll_name)

    def support_timer_process(self):
        print("========= Support is RETURN")
        self.SupportReturn = True

    def update_support_time(self):
        self.back_to_base()
        timeout = 0
        while not self.confirm("主界面", "btn_远征信息"):
            time.sleep(1)
            timeout += 1
            if timeout > 10:
                self.back_to_base()
                timeout = 0

        self.click_ex("主界面", "btn_远征信息", delay=[800, 300], stuck_mode=[False, True],
                      after_confirm=["远征信息界面", "state_confirm_1", None, True])

        SupportNum = 4
        for i in range(SupportNum):
            text_name = "text_远征" + str(i + 1)
            __rect = self.ROIs["远征信息界面"][text_name]
            timeout = 0
            print("text_ocr() Start: ")
            while True:
                if not self.confirm("远征信息界面", "state_confirm_1", capture_rect_scale=1.2):
                    return False
                _image = cv2.cvtColor(self.cap.capture(__rect), cv2.COLOR_RGBA2RGB)
                time_text = opencv.text_ocr(_image, '', '0123456789:').replace(' ', '').split(':')
                try:
                    second = duration_to_second(int(time_text[0]), int(time_text[1]), int(time_text[2])) + 10
                    break
                except (ValueError, IndexError):
                    # print("text_ocr() failed! Retry!! OCR result: ", time_text)
                    timeout += 1
                    if timeout > 60:
                        return False
                    continue
            pass
            print("text_ocr() succeed!! OCR result: ", second)
            self.SupportRemainTime[i] = second
            self.SupportTimer[i].cancel()
            self.SupportTimer[i] = threading.Timer(second, self.support_timer_process)
            self.SupportTimer[i].start()
        self.remove_close_timer(SupportNum)
        self.click_ex("远征信息界面", "btn_远征信息", after_confirm=["主界面", "btn_远征信息", None, True])
        return True

    def remove_close_timer(self, SupportNum):
        sorted = np.sort(self.SupportRemainTime)[::-1]  # Descending order
        time_diff = np.ones(SupportNum - 1) * -9999
        for i in range(SupportNum - 1):
            time_diff[i] = sorted[i] - sorted[i + 1]
        filtered = np.where(time_diff <= 30)[0]
        clusters = self.clustering_continue_number(filtered)
        # Only reserve the longest time at one cluster, so I have to stop the timer that is not the longest.
        clusters_remove = []
        for one_cluster in clusters:
            one_cluster.append(one_cluster[-1] + 1)
            one_cluster.pop(0)  # Remove the longest time.
            clusters_remove += one_cluster  #
        for sorted_idx in clusters_remove:
            remove_idx = self.SupportRemainTime.index(sorted[sorted_idx])
            self.SupportTimer[remove_idx].cancel()

    def clustering_continue_number(self, sorted_list=None):
        if (sorted_list is []) or (sorted_list is None):
            return []
        cluster = []
        one_cluster = []
        for i in range(sorted_list.__len__() - 1):
            one_cluster += [sorted_list[i]]
            if sorted_list[i + 1] - sorted_list[i] == 1:
                if i == sorted_list.__len__() - 2:
                    one_cluster.append(sorted_list[i + 1])
                    cluster.append(one_cluster)
                pass
            else:
                cluster.append(one_cluster)
                one_cluster = []
        return cluster

    def get_HP(self, slot):
        scene_name = "编成界面"
        confirm1_name = "state_confirm_1"
        ocr_name = "text_HP_Slot" + str(slot)
        if self.confirm(scene_name, confirm1_name, capture_rect_scale=1.2, watch=False):
            __rect = self.ROIs[scene_name][ocr_name]
            _image = cv2.cvtColor(self.cap.capture(__rect), cv2.COLOR_RGB2GRAY)
            ret, binnary = cv2.threshold(_image, 200, 255, cv2.THRESH_OTSU)
            HP = int(np.round(binnary.sum() / 255))
            All_HP = binnary.size
            return (HP / All_HP) * 100.0
        return -1

    def get_doll_state(self, state_name='HP', slot_idx=1, doll_name='FAL'):
        '''
        :param state_name: 'HP', 'Ammo', 'Food'
        :param slot_idx: 1~5
        param doll_name: "FAL","M4A1","GrG11","Zas","416","SOPMODII"
        :return: Percentage of state
        '''
        if self.getState("战斗梯队部署界面")[0] != '战斗梯队部署界面':
            print("get_doll_state(): ", "getState() error")
            return 0
        if state_name in ['HP', 'Ammo', 'Food']:
            slot_name = "btn_" + state_name + "_Slot" + str(slot_idx)
            state_img = self.cap.capture(self.ROIs["战斗梯队部署界面"][slot_name])
            _gray = cv2.cvtColor(state_img, cv2.COLOR_RGB2GRAY)
            NoUse, binnary = cv2.threshold(_gray, 60, 255, cv2.ADAPTIVE_THRESH_MEAN_C)
            # MakeRECT.cv2_imshow(slot_name, _gray)
            count = int(np.round(binnary.sum() / 255))
            All_count = binnary.size
            if state_name == 'HP':
                res = np.round((count / All_count) * 100.0)
            else:
                res = np.round((count / All_count) * 100.0, -1)
            return int(res)
        elif state_name == 'Name':
            slot_name = "btn_" + state_name + "_Slot" + str(slot_idx)
            return self.confirm("战斗梯队部署界面", slot_name, "部署_Gun_" + doll_name)
        else:
            print("get_doll_state(): ", "Parameter [state_name] error")
            return 0

    def find_fighter(self, fighters, slot_idx=2):
        print("Finding the fighter.")
        ammo = self.get_doll_state("Ammo", slot_idx)
        food = self.get_doll_state("Food", slot_idx)
        print("The fighter's condition is: Ammo: %d ; Food: %d" % (ammo, food))
        battle_condition = ammo > 90 and food > 60
        fighter_name = "None!"

        if not battle_condition:
            print("Change the fighter.")
            for fighter in fighters:
                if self.get_doll_state("Name", slot_idx, doll_name=fighter) == True:
                    fighter_name = fighters[(fighters.index(fighter) + 1) % (len(fighters))]
                    print("The fighter is ", fighter_name)
                    return fighter_name
        else:
            print("Not change the fighter.")
            return "NoChange"
        # if not battle_condition:
        #     if self.get_doll_state("Name", slot_idx, doll_name="FAL") == True:
        #         fighter_name = "G11"
        #     elif self.get_doll_state("Name", slot_idx, doll_name="G11") == True:
        #         fighter_name = "FAL"
        #     else:
        #         self.send_fault_and_stop_program()
        # else:
        #     if self.get_doll_state("Name", slot_idx, doll_name="FAL") == True:
        #         fighter_name = "FAL"
        #     elif self.get_doll_state("Name", slot_idx, doll_name="G11") == True:
        #         fighter_name = "G11"
        #     else:
        #         self.send_fault_and_stop_program()
        # print("The fighter is ", fighter_name)
        # return fighter_name

    def repair_battle(self, slot):
        btn_name = "btn_Repair_Slot" + str(slot)
        self.click_ex("战斗梯队部署界面", btn_name, pre_confirm=["战斗梯队部署界面", "state_confirm_1", None, True]
                      , after_confirm=["部署前修复界面", "btn_Confirm", None, True], stuck_mode=[False, True])

        return self.click_ex("部署前修复界面", "btn_Confirm", after_confirm=["战斗梯队部署界面", "state_confirm_1", None, True],
                             stuck_mode=[False, True])

    # scene_name = "战斗梯队部署界面"
    # confirm1_name = "state_confirm_1"
    # btn_name = "btn_Repair_Slot" + str(slot)
    # if self.confirm(scene_name, confirm1_name):
    # 	__rect = self.ROIs[scene_name][btn_name]
    # 	self.op.click_in_rect(__rect, [600, 80])
    # 	__rect = self.ROIs['部署前修复界面']['btn_Confirm']
    # 	self.op.click_in_rect(__rect, [600, 100])
    # 	return True
    # return False

    def zoom_out_init(self, times=9):
        for i in range(times):
            self.op.f5_key_zoom_out()
            self.op.rnd_sleep(2000, 300)
    
    def close_app(self):
        self.op.press_change_app()
        self.op.rnd_sleep(1300, 300)
        self.op.click_in_rect([1332, 212, 53, 56])
        
    def restart_app(self):
        self.close_app()
        self.op.rnd_sleep(2000, 300)
        if self.process_StartUp2() == False:
            return False
    
    def zoom_confirm(self, scene_name, rect_name_list, capture_rect_scale=2, threashold=0.3):
        res = 1
        print("Zoom confirmation:")
        for name in rect_name_list:
            tmp = [0]  # Use reference to get the variable inside of the [confirm] function.
            self.confirm(scene_name, name, capture_rect_scale=capture_rect_scale, confirm_res=tmp)
            res *= tmp[0]
        print(res)
        if res > threashold:
            return True
        else:
            return False
        
    def do_simulation(self):
        self.TimeToSimulation = False
        self.back_to_base()
        if datetime.datetime.isoweekday(datetime.datetime.now()) in [2, 5, 7]:
            self.simulation_data(level="middle")
        else:
            self.simulation_mental(level="high")
        self.update_simulation_timer()
        self.back_to_base()
        
        
    def enter_simulation(self, type="data"):
        TYPE_IMG_DICT = {
            "capsule": "强化",
            "data": "资料",
            "exp": "经验",
            "mental": "心智",
            "defense": "防御",
            "fusion": "融合",
        }
        while self.select_battle_type("simulate") == False:
            if self.getState("主界面-无活动")[0] == "主界面-无活动" or self.getState("主界面")[0] == "主界面":
                self.click_Battle()
            time.sleep(2)
            self.处理远征()
            
        self.click_ex("战斗选择界面_模拟", "btn_" + type, pre_confirm=["战斗选择界面", "state_confirm_1", None, True]
                      , after_confirm=["战斗选择界面_模拟_" + TYPE_IMG_DICT[type], "state_confirm_1", None, True], stuck_mode=[False, True])
        return True
        # time_info, ap_info = self.simulation_get_AP_info()
        # if ap_info[0] < 3:
        #     print("Current AP is ", ap_info[0], " less than 3. EXIT!!")
        #     return False
        
    
    def simulation_get_AP_info(self):
        MAX_AP = 6
        hour = 0
        min = 0
        sec = 0
        cur = 0
        max = 0
        _rect = self.ROIs["战斗选择界面_模拟_资料"]["text_time"]
        timeout = 50
        while timeout:
            _image = cv2.cvtColor(self.cap.capture(_rect), cv2.COLOR_RGBA2RGB)
            # cv2.invert(_image, _image)
            time_text = opencv.text_ocr(_image, lang="eng", char_whitelist="012345679:/", padding=15, inverse=True, erode=True, iterations=0).split(":")
            print("text_time:", time_text)
            try:
                hour = int(time_text[0])
                min = int(time_text[1])
                sec = int(time_text[2])
                if (not (0 <= hour <= 2)) or (not (0 <= min < 60)) or (not (0 <= sec < 60)):
                    raise ValueError
                break
            except:
                timeout -= 1
                continue
        if timeout == 0:
            hour = 0
            min = 0
            sec = 0
            
        timeout = 50
        _rect = self.ROIs["战斗选择界面_模拟_资料"]["text_AP"]  #
        while timeout:
            _image = cv2.cvtColor(self.cap.capture(_rect), cv2.COLOR_RGBA2RGB)
            AP_text = opencv.text_ocr(_image, lang="eng", char_whitelist="0123456", padding=10, psm_para="6",
                                        inverse=True, erode=True, iterations=1)
            print("text_AP:", AP_text)
            try:
                cur = int(AP_text)
                if (not (0 <= cur <= MAX_AP)):
                    raise ValueError
                break
            except:
                timeout -= 1
                continue
        if timeout == 0:
            cur = 4  # Check after 2 hours.
        
        hour += (MAX_AP - cur - 1) * 2  #
        return [hour, min, sec], [cur]
    
    def simulation_timer_process(self):
        print("========= It is time to do the simulation")
        self.TimeToSimulation = True
        
    def update_simulation_timer(self):
        while self.select_battle_type("simulate") == False:
            if self.getState("主界面-无活动")[0] == "主界面-无活动" or self.getState("主界面")[0] == "主界面":
                self.click_Battle()
            time.sleep(2)
            self.处理远征()
            
        # time_info, ap_info = self.simulation_get_AP_info()
        # second = (time_info[0] - 1) * 3600 + time_info[1] * 60 + time_info[2]  # The simulation should be done before the AP are full
        print("Set simulation timer to 7200 seconds")
        second = 7200
        self.SimulationTimer[0].cancel()
        self.SimulationTimer[0] = threading.Timer(second, self.simulation_timer_process)
        self.SimulationTimer[0].start()
    
    def simulation_data(self, level="high"):
        '''
        
        :param level: high, middle, low
        :return:
        '''
        if not self.enter_simulation("data"):
            return
        
        # self.click_ex("战斗选择界面_模拟_资料", "btn_" + level, pre_confirm=["战斗选择界面", "state_confirm_1", None, True]
        #               , after_confirm=["战斗选择界面_模拟_资料_准备", "state_confirm_1", None, True], stuck_mode=[False, True])
        while True:
            self.click_ex("战斗选择界面_模拟_资料", "btn_" + level, pre_confirm=["战斗选择界面", "state_confirm_1", None, True]
                          , stuck_mode=[False, False])
            if self.confirm("战斗选择界面_模拟_AP不足", "state_confirm_1"):
                self.click_ex("战斗选择界面_模拟_AP不足", "btn_return")
                return False
            elif self.confirm("战斗选择界面_模拟_资料_准备", "state_confirm_1"):
                break
        self.click_ex("战斗选择界面_模拟_资料_准备", "btn_start", pre_confirm=["战斗选择界面_模拟_资料_准备", "state_confirm_1", None, True]
                      , after_confirm=["战斗选择界面_模拟_资料_出击确认", "state_confirm_1", None, True], stuck_mode=[False, True])
        self.click_ex("战斗选择界面_模拟_资料_出击确认", "btn_confirm", pre_confirm=["战斗选择界面_模拟_资料_出击确认", "state_confirm_1", None, True]
                      , stuck_mode=[False, True])
        while True:
            time.sleep(15)
            self.click_ex("战斗中", "btn_click", delay=[200, 30])
            self.click_ex("战斗中", "btn_click", delay=[200, 30])
            self.click_ex("战斗中", "btn_click", delay=[200, 30])
            if self.click_ex("战斗选择界面_模拟_资料_出击确认", "btn_confirm",
                          pre_confirm=["战斗选择界面_模拟_资料_出击确认", "state_confirm_1", None, True], stuck_mode=[False, False]):
                continue
            else:
                break
        self.waitting_for_scene("战斗选择界面_模拟_资料")
    
    def get_effectiveness(self, night=False):
        """
        在梯队部署界面取得当前梯队的效能.
        :return:
        """
        if night:
            rect = self.ROIs["战斗梯队部署界面"]["text_night_effectiveness"]
        else:
            rect = self.ROIs["战斗梯队部署界面"]["text_effectiveness"]
        _rect = copy.deepcopy(rect)
        effect = 0
        while True:
            _image = cv2.cvtColor(self.cap.capture(_rect), cv2.COLOR_RGBA2RGB)
            effect_text = []
            effect_text.append(opencv.text_ocr(_image, lang="eng", char_whitelist="0123456789", padding=15, erode=True, iterations=0))
            print("res:", effect_text)
            _exit = False
            for res in effect_text:
                try:
                    effect = int(res)
                    if effect < 100:
                        raise ValueError
                    _exit = True
                    break
                except:
                    continue
            if _exit:
                break
            
        return effect
    
    def find_all_rect_in_big_rect(self, scene_name, search_range_rect, finding_rect, confirm_imglist):
        res_dict = {}
        for confirm_img in confirm_imglist:
            ret = self.find_rect_in_big_rect(scene_name=scene_name,
                                       search_range_rect=search_range_rect,
                                       finding_rect=finding_rect,
                                       confirm_img_name=confirm_img)
            if ret == False:
                continue
            else:
                res_dict.update({confirm_img: ret})
        return res_dict
    
    def find_echelon_by_effect(self, effect=10000):
        """
        No scroll
        :param effect: 
        :return: 
        """
        confirm_img_list = [
                            "echelon_1_Selected",
                            "echelon_1_Unselected",
                            "echelon_2_Selected",
                            "echelon_2_Unselected",
                            "echelon_3_Selected",
                            "echelon_3_Unselected",
                            "echelon_4_Selected",
                            "echelon_4_Unselected",
                            "echelon_5_Selected",
                            "echelon_5_Unselected",
                            "echelon_6_Selected",
                            "echelon_6_Unselected",
                            "echelon_7_Selected",
                            "echelon_7_Unselected",
                            "echelon_8_Selected",
                            "echelon_8_Unselected",
                            "echelon_9_Selected",
                            "echelon_9_Unselected",
                            "echelon_10_Selected",
                            "echelon_10_Unselected",
                            "echelon_friend_Unselected"
                            ]
        valid_echelon_dict = self.find_all_rect_in_big_rect("战斗梯队部署界面", "text_echelon_finding", "btn_echelon_1", confirm_img_list)
        for echelon in valid_echelon_dict:
            self.click_ex("战斗梯队部署界面", "", click_roi=valid_echelon_dict[echelon])
            if self.get_effectiveness() < effect:
                continue
            else:
                return True
        return False
    
    def battle_waitting(self, times):
        battle_count = 0
        while True:
            # 战斗之间
            while not self.confirm("战斗中", "state_confirm_1"):
                self.op.rnd_sleep(2000, 300)
        
            # 战斗中
            while self.confirm("战斗中", "state_confirm_1"):
                self.op.rnd_sleep(700, 300)
        
            battle_count += 1
            # 战斗结束，快速点击屏幕
            self.op.rnd_sleep(900, 100)
            for i in range(7):
                self.click_ex("战斗中", "btn_click", delay=[200, 30])
            if battle_count >= times:
                break
    
    def waitting_for_scene(self, scene_name, roi_name="state_confirm_1"):
        while self.confirm(scene_name, roi_name) == False:
            self.op.rnd_sleep(100, 30)
        self.op.rnd_sleep(3000, 30)
    
    def simulation_mental(self, level="high", effect_threshold=23000):
        """
        The screen is in the [mental] page.
        :param level:
        :param effect_threshold:
        :return:
        """
        if not self.enter_simulation("mental"):
            return
        
        # self.click_ex("战斗选择界面_模拟_心智", "btn_" + level, pre_confirm=["战斗选择界面", "state_confirm_1", None, True], stuck_mode=[False, True])
        
        while True:
            self.click_ex("战斗选择界面_模拟_心智", "btn_" + level, pre_confirm=["战斗选择界面", "state_confirm_1", None, True]
                          , stuck_mode=[False, False])
            if self.confirm("战斗选择界面_模拟_AP不足", "state_confirm_1"):
                self.click_ex("战斗选择界面_模拟_AP不足", "btn_return")
                return False
            elif self.getState("作战地图0_2") == []:
                self.op.rnd_sleep(1000, 30)
                continue
            else:
                break
        
        while self.getState("作战地图0_2") == []:
            self.op.rnd_sleep(100, 30)
        self.op.rnd_sleep(3000, 30)
        if self.zoom_confirm("作战地图_模拟_高级", ["btn_司令部", "btn_tmp1", "btn_tmp2", "btn_tmp3", "btn_终点"]) == False:
            self.zoom_out_init(9)
            self.click_ex("作战地图_模拟_高级", "btn_Return", pre_confirm=["作战地图_模拟_高级", "state_confirm_1", None, True],
                          stuck_mode=[False, True])
            self.op.rnd_sleep(5000, 30)
            self.click_ex("战斗选择界面_模拟_心智", "btn_" + level, pre_confirm=["战斗选择界面", "state_confirm_1", None, True],
                          stuck_mode=[False, True])

        self.click_ex("作战地图_模拟_高级", "btn_司令部", after_confirm=["战斗梯队部署界面", "state_confirm_1", None, True],
                      stuck_mode=[False, True])
        self.find_echelon_by_effect(effect_threshold)
        self.click_ex("战斗梯队部署界面", "btn_Confirm", after_confirm=["作战地图_模拟_高级", "state_confirm_1", None, True],
                      stuck_mode=[False, True])
        
        # Click BattleStart and confirm use the AP.
        self.click_ex("作战地图0_2", "btn_BattleStart", [1000, 400], stuck_mode=[False, True], click_cnt=2,
                    after_confirm=["战斗选择界面_模拟_资料_出击确认", "state_confirm_1", None, True], )
        self.click_ex("战斗选择界面_模拟_资料_出击确认", "btn_confirm",
                      pre_confirm=["战斗选择界面_模拟_资料_出击确认", "state_confirm_1", None, True],
                      after_confirm=["作战地图0_2", "btn_PlanExec", "btn_TurnEnd", True],
                      stuck_mode=[False, False])

        self.op.rnd_sleep(3000, 30)
        self.click("作战地图_模拟_高级", "btn_司令部", [700, 200])
        self.click("作战地图_模拟_高级", "btn_PlanMode", [700, 200])
        self.click("作战地图_模拟_高级", "btn_司令部", [700, 200])
        self.click("作战地图_模拟_高级", "btn_终点", [700, 200])
        self.click("作战地图_模拟_高级", "btn_PlanExec", [700, 200])
        
        while True:
            self.battle_waitting(1)
    
            time.sleep(15)
            self.click_ex("战斗中", "btn_click", delay=[200, 30])
            self.click_ex("战斗中", "btn_click", delay=[200, 30])
            self.click_ex("战斗中", "btn_click", delay=[200, 30])
            
            while True:
                if self.confirm("战斗选择界面_模拟_资料_出击确认", "state_confirm_1"):
                    self.click_ex("战斗选择界面_模拟_资料_出击确认", "btn_confirm",
                                  pre_confirm=["战斗选择界面_模拟_资料_出击确认", "state_confirm_1", None, True],
                                  stuck_mode=[False, False])
                    break
                elif self.confirm("战斗选择界面", "state_confirm_1"):
                    return
                else:
                    time.sleep(3)
                    self.click_ex("战斗中", "btn_click", delay=[200, 30])
                    self.click_ex("战斗中", "btn_click", delay=[200, 30])
                    self.click_ex("战斗中", "btn_click", delay=[200, 30])
            
            
        
    def Test(self):
        # self.simulation_get_AP_info()
        self.do_simulation()
        while True:
            time.sleep(2)


if __name__ == "__main__":
    DF = DollFrontlineBase()
    DF.Test()
