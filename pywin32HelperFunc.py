import cv2
import numpy as np
import tesserocr
import win32api, win32ui, win32gui, win32con
from PIL import Image
import pyautogui
from MakeRECT import cv2_imshow


class Win32_Capture:
	
	def __init__(self, WindowName, SubWindowName=None):
		MainWindowHandle = win32gui.FindWindow(None, WindowName)
		GameChildWindowHandle = FindGameChildWindowHandle_byText(MainWindowHandle, SubWindowName)
		self.main_hwnd = MainWindowHandle
		if SubWindowName is None:
			self.hwnd = MainWindowHandle
		else:
			self.hwnd = GameChildWindowHandle
		
		win32gui.MoveWindow(MainWindowHandle, 280, -1080, 1920, 1080, True)
		win32gui.MoveWindow(self.hwnd, 0, -0, 1920, 1080, True)
		# aaa=win32gui.GetWindowRect(self.hwnd)
		# bbb=win32gui.GetWindowRect(MainWindowHandle)
		# 根据窗口句柄获取窗口的设备上下文DC（Divice Context）
		self.hwndDC = win32gui.GetWindowDC(self.hwnd)
		# 根据窗口的DC获取mfcDC
		self.mfcDC = win32ui.CreateDCFromHandle(self.hwndDC)
		# mfcDC创建可兼容的DC
		self.saveDC = self.mfcDC.CreateCompatibleDC()
		# 创建bitmap准备保存图片
		self.saveBitMap = win32ui.CreateBitmap()
		# 获取显示器信息
		self.MonitorDev = win32api.EnumDisplayMonitors(None, None)
		# 为bitmap开辟空间
		self.saveBitMap.CreateCompatibleBitmap(self.mfcDC, self.MonitorDev[0][2][2], self.MonitorDev[0][2][3])
		# 高度saveDC，将截图保存到saveBitmap中
		self.saveDC.SelectObject(self.saveBitMap)
		
	def __del__(self):
		win32gui.DeleteObject(self.saveBitMap.GetHandle())
		self.saveDC.DeleteDC()
		
	def capture(self, rect=None, return_cv2_mat=True):
		win32gui.MoveWindow(self.main_hwnd, 280, -1080, 1920, 1080, True)
		win32gui.MoveWindow(self.hwnd, 0, -0, 1920, 1080, True)
		x = 0
		y = 0
		w = 0
		h = 0
		if rect is None:
			# w = self.MonitorDev[0][2][2]
			# h = self.MonitorDev[0][2][3]
			w = 1920
			h = 1080
			rect = [0, 0, w, h]
			self.saveDC.BitBlt((0, 0), (w, h), self.mfcDC, (0, 0), win32con.SRCCOPY)
		else:
			try:
				x = rect[0]
				y = rect[1]
				w = rect[2]
				h = rect[3]
				if (w is 0) or (h is 0):
					raise ValueError
				if (w < 0) or (h < 0):
					raise ValueError
				if (x > 1920) or (y > 1080):
					raise ValueError
				if (x < 0) or (y < 0):
					raise ValueError
				if ((x + w) - 1 > 1920) or ((y + h) - 1 > 1080):
					raise ValueError
			except ValueError or IndexError:
				print("capture error!")
		while True:
			error_DeleteObject = False
			error_CreateCompatibleBitmap = False
			error_SelectObject = False
			error_BitBlt = False
			try:
				win32gui.DeleteObject(self.saveBitMap.GetHandle())
				error_DeleteObject = True
				self.saveBitMap.CreateCompatibleBitmap(self.mfcDC, w, h)
				error_CreateCompatibleBitmap = True
				self.saveDC.SelectObject(self.saveBitMap)
				error_SelectObject = True
				self.saveDC.BitBlt((0, 0), (w, h), self.mfcDC, (x, y), win32con.SRCCOPY)
				error_BitBlt = True
				break
			except:
				print("========================================")
				if error_DeleteObject == True:
					print("== error_DeleteObject")
				if error_CreateCompatibleBitmap == True:
					print("== error_CreateCompatibleBitmap")
				if error_SelectObject == True:
					print("== error_SelectObject")
				if error_BitBlt == True:
					print("== error_BitBlt")
				print("== rect: ", rect)
				print("== x: ", x, "; y: ", y, "; weight: ", w, "; height: ", h)
				print("== capture(): error!")
				print("========================================")
				continue
		if return_cv2_mat:
			im = self.saveBitMap.GetBitmapBits(False)
			ret_im = np.array(im, dtype="uint8")
			shape = (rect[3], rect[2], 4)
			reshaped = ret_im.reshape(shape)
			# cv2_imshow("captured", reshaped)
			return reshaped
		else:
			return None


def callback_EnumChildWindows(hwnd, ext):
	ext.append(hwnd)
	

def FindGameChildWindowHandle_byText(hwndMainWindow, childWindowText):
	child_hwnd_res = []
	win32gui.EnumChildWindows(hwndMainWindow, callback_EnumChildWindows, child_hwnd_res)
	for childHwnd in child_hwnd_res:
		if win32gui.GetWindowText(childHwnd) == childWindowText:
			return childHwnd


def window_capture_cont(hwnd, pos=None, needShow=False):
	hwnd = hwnd  # 窗口的编号，0号表示当前活跃窗口
	# 根据窗口句柄获取窗口的设备上下文DC（Divice Context）
	hwndDC = win32gui.GetWindowDC(hwnd)
	# 根据窗口的DC获取mfcDC
	mfcDC = win32ui.CreateDCFromHandle(hwndDC)
	# mfcDC创建可兼容的DC
	saveDC = mfcDC.CreateCompatibleDC()
	# 创建bigmap准备保存图片
	saveBitMap = win32ui.CreateBitmap()
	# 获取监控器信息
	MoniterDev = win32api.EnumDisplayMonitors(None, None)
	if pos == None:
		x1 = 0
		y1 = 0
		w = MoniterDev[0][2][2]
		h = MoniterDev[0][2][3]
	else:
		x1 = pos[0]
		y1 = pos[1]
		w = pos[2] - pos[0]
		h = pos[3] - pos[1]
	# print w,h　　　#图片大小
	# 为bitmap开辟空间
	saveBitMap.CreateCompatibleBitmap(mfcDC, MoniterDev[0][2][2], MoniterDev[0][2][3])
	# img = np.zeros(h * w * 4)
	
	while(True):
		# 高度saveDC，将截图保存到saveBitmap中
		# saveDC.SelectObject(saveBitMap)
		# 截取从左上角（0，0）长宽为（w，h）的图片
		# saveDC.BitBlt((x1, y1), (w, h), mfcDC, (x1, y1), win32con.SRCCOPY)
		# saveBitMap.SaveBitmapFile(saveDC, filename)
		img = np.array(pyautogui.screenshot())
		# im = saveBitMap.GetBitmapBits(False)
		# img = np.array(im).astype(dtype="uint8").reshape((h, w, 4))
		img = cv2.cvtColor(img, cv2.COLOR_RGBA2BGR)
	
		
		cv2.imshow("res", img)
		key = cv2.waitKey(10)
		if key == 'q':
			cv2.destroyAllWindows()


def capture_set_rects(hwnd, pos=None):
	ROI_dict = {}
	hwnd = hwnd  # 窗口的编号，0号表示当前活跃窗口
	# 根据窗口句柄获取窗口的设备上下文DC（Divice Context）
	hwndDC = win32gui.GetWindowDC(hwnd)
	# 根据窗口的DC获取mfcDC
	mfcDC = win32ui.CreateDCFromHandle(hwndDC)
	# mfcDC创建可兼容的DC
	saveDC = mfcDC.CreateCompatibleDC()
	# 创建bigmap准备保存图片
	saveBitMap = win32ui.CreateBitmap()
	# 获取监控器信息
	MoniterDev = win32api.EnumDisplayMonitors(None, None)
	if pos == None:
		x1 = 0
		y1 = 0
		w = MoniterDev[0][2][2]
		h = MoniterDev[0][2][3]
	else:
		x1 = pos[0]
		y1 = pos[1]
		w = pos[2] - pos[0]
		h = pos[3] - pos[1]
	# print w,h　　　#图片大小
	# 为bitmap开辟空间
	saveBitMap.CreateCompatibleBitmap(mfcDC, MoniterDev[0][2][2], MoniterDev[0][2][3])
	# 高度saveDC，将截图保存到saveBitmap中
	saveDC.SelectObject(saveBitMap)
	# 截取从左上角（0，0）长宽为（w，h）的图片
	saveDC.BitBlt((x1, y1), (w, h), mfcDC, (x1, y1), win32con.SRCCOPY)
	# saveBitMap.SaveBitmapFile(saveDC, filename)
	
	im = saveBitMap.GetBitmapBits(False)
	img = np.array(im).astype(dtype="uint8")
	img.shape = (h, w, 4)
	img = cv2.cvtColor(img, cv2.COLOR_RGBA2GRAY)
	
	cv2.imshow("res", img)
	croped = cv2.selectROI("res", img, )
	cv2.waitKey()
	cv2.destroyAllWindows()
	imCrop = img[int(croped[1]):int(croped[1] + croped[3]), int(croped[0]):int(croped[0] + croped[2])]
	
	# 清楚图片数据，防止内存泄露
	win32gui.DeleteObject(saveBitMap.GetHandle())
	saveDC.DeleteDC()


def window_capture(hwnd, pos=None, needShow=False):
	ROI_dict = {}
	hwnd = hwnd  # 窗口的编号，0号表示当前活跃窗口
	# 根据窗口句柄获取窗口的设备上下文DC（Divice Context）
	hwndDC = win32gui.GetWindowDC(hwnd)
	# 根据窗口的DC获取mfcDC
	mfcDC = win32ui.CreateDCFromHandle(hwndDC)
	# mfcDC创建可兼容的DC
	saveDC = mfcDC.CreateCompatibleDC()
	# 创建bigmap准备保存图片
	saveBitMap = win32ui.CreateBitmap()
	# 获取监控器信息
	MoniterDev = win32api.EnumDisplayMonitors(None, None)
	if pos == None:
		x1 = 0
		y1 = 0
		w = MoniterDev[0][2][2]
		h = MoniterDev[0][2][3]
	else:
		x1 = pos[0]
		y1 = pos[1]
		w = pos[2] - pos[0]
		h = pos[3] - pos[1]
	# print w,h　　　#图片大小
	# 为bitmap开辟空间
	saveBitMap.CreateCompatibleBitmap(mfcDC, MoniterDev[0][2][2], MoniterDev[0][2][3])
	# 高度saveDC，将截图保存到saveBitmap中
	saveDC.SelectObject(saveBitMap)
	# 截取从左上角（0，0）长宽为（w，h）的图片
	saveDC.BitBlt((x1, y1), (w, h), mfcDC, (x1, y1), win32con.SRCCOPY)
	# saveBitMap.SaveBitmapFile(saveDC, filename)
	
	im = saveBitMap.GetBitmapBits(False)
	img = np.array(im).astype(dtype="uint8")
	img.shape = (h, w, 4)
	img = cv2.cvtColor(img, cv2.COLOR_RGBA2GRAY)
	
	if needShow:
		cv2.imshow("res", img)
		cv2.waitKey()
		cv2.destroyWindow("res")
	
	cv2.namedWindow("res", cv2.WINDOW_KEEPRATIO)
	cv2.imshow("res", img)
	croped = cv2.selectROI("res", img, )
	cv2.waitKey()
	cv2.destroyAllWindows()
	imCrop = img[int(croped[1]):int(croped[1] + croped[3]), int(croped[0]):int(croped[0] + croped[2])]
	
	# binnary = imCrop
	ret, binnary = cv2.threshold(imCrop, 200, 255, cv2.THRESH_OTSU)
	cv2.namedWindow("binmg", cv2.WINDOW_KEEPRATIO)
	cv2.imshow("binmg", binnary)
	cv2.waitKey()
	cv2.destroyAllWindows()
	
	ocr = Image.fromarray(binnary)
	rrr0 = tesserocr.image_to_text(ocr)
	rrr1 = tesserocr.image_to_text(ocr, lang='eng')
	rrr2 = tesserocr.image_to_text(ocr, lang='jpn')
	rrr4 = tesserocr.image_to_text(ocr, lang='chi_sim')
	
	# 清楚图片数据，防止内存泄露
	win32gui.DeleteObject(saveBitMap.GetHandle())
	saveDC.DeleteDC()


def find_picture(target, template):
	# 获得模板图片的高宽尺寸
	theight, twidth = template.shape[:2]
	# 执行模板匹配，采用的匹配方式cv2.TM_SQDIFF_NORMED
	result = cv2.matchTemplate(target, template, cv2.TM_SQDIFF_NORMED)
	# 归一化处理
	cv2.normalize(result, result, 0, 1, cv2.NORM_MINMAX, -1)
	# 寻找矩阵（一维数组当做向量，用Mat定义）中的最大值和最小值的匹配结果及其位置
	min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
	# 匹配值转换为字符串
	# 对于cv2.TM_SQDIFF及cv2.TM_SQDIFF_NORMED方法min_val越趋近与0匹配度越好，匹配位置取min_loc
	# 对于其他方法max_val越趋近于1匹配度越好，匹配位置取max_loc
	strmin_val = str(min_val)
	# 绘制矩形边框，将匹配区域标注出来
	# min_loc：矩形定点
	# (min_loc[0]+twidth,min_loc[1]+theight)：矩形的宽高
	# (0,0,225)：矩形的边框颜色；2：矩形边框宽度
	cv2.rectangle(target, min_loc, (min_loc[0] + twidth, min_loc[1] + theight), (0, 0, 225), 2)
	# 显示结果,并将匹配值显示在标题栏上
	cv2.imshow("MatchResult----MatchingValue="+strmin_val,target)
	cv2.waitKey()
	cv2.destroyAllWindows()
	x = min_loc[0]
	y = min_loc[1]
	
	return x, y


# target原始图片
# x,y 起始坐标
# w,h 返回的宽长
def get_pic_from_pic(x, y, w, h, target):
	region = target[y:y + h, x:x + w]
	return region


def compare_picture(imageA, imageB):
	# 灰度图片比较
	grayA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
	grayB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)
	
	(score, diff) = find_picture(grayA, grayB)
	return float(score)


def capture_and_set_ROIs(hdwn):
	嘿嘿 = 33
	pass


# MainWindowHandle = win32gui.FindWindow(None, "雷电模拟器")
# GameChildWindowHandle = FindGameChildWindowHandle_byText(MainWindowHandle, "TheRender")
# window_capture(GameChildWindowHandle)

