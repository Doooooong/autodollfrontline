import win32api, win32ui, win32gui, win32con
import pywin32HelperFunc as win32
import time
import cv2
import numpy as np


# adb 命令： dumpsys activity | grep mFocusedActivity
desired_caps = {
"platformName": "Android",
"deviceName": "Phone",
"fullReset": "False",
"no-reset": "True",
"newCommandTimeout": "86400",
}


class MouseOperation:
	DRAG_DELTA = 5
	DRAG_VECTOR_DELTA = np.deg2rad(10)
	DRAG_DELTA_RANGE = 2  # DRAG_DELTA 的正负波动范围
	DRAG_TIME_RANGE = 50
	CLICK_TIME_RANGE = 10
	DRAG_TIME = 400
	
	def __init__(self, WindowName, SubWindowName=None):
		MainWindowHandle = win32gui.FindWindow(None, WindowName)
		GameChildWindowHandle = win32.FindGameChildWindowHandle_byText(MainWindowHandle, SubWindowName)
		if SubWindowName is None:
			self.hwnd = MainWindowHandle
		else:
			self.hwnd = GameChildWindowHandle
		
		pass
	
	def get_rand_at_range(self, low=0, high=1, shape=None):
		if shape is None:
			return np.random.random() * (high - low) + low
		else:
			return np.random.random(shape) * (high - low) + low
	
	def get_normal_rand_at_range(self, low=0, high=1, shape=None):
		if shape is None:
			np.random.normal()
			return np.random.random() * (high - low) + low
		else:
			return np.random.random(shape) * (high - low) + low
		
	def rnd_sleep(self, base_time=100, rnd_range=10):
		time.sleep(base_time / 1000)
		_time = self.get_rand_at_range(0, rnd_range)
		time.sleep(_time / 1000)
	
	def click_in_rect(self, rect, delay=None):
		if delay is None:
			delay = [100, 10]
		x = self.get_rand_at_range(rect[0], rect[0] + rect[2])
		y = self.get_rand_at_range(rect[1], rect[1] + rect[3])
		tmp = win32api.MAKELONG(int(x), int(y))
		win32gui.PostMessage(self.hwnd, win32con.WM_LBUTTONDOWN, win32con.MK_LBUTTON, tmp)
		self.rnd_sleep(10, MouseOperation.CLICK_TIME_RANGE)
		win32gui.PostMessage(self.hwnd, win32con.WM_LBUTTONUP, win32con.MK_LBUTTON, tmp)
		self.rnd_sleep(base_time=delay[0], rnd_range=delay[1])
		pass
	
	def move_mouse(self, pos=None):
		if pos is None:
			pos = [0, 0]
		_tmp = win32api.MAKELONG(int(pos[0]), int(pos[1]))
		win32gui.PostMessage(self.hwnd, win32con.WM_MOUSEMOVE, 0, _tmp)
	
	def move_mouse_rnd(self, rect=None):
		if rect is None:
			rect = [0, 0, 1920, 1080]
		x = self.get_rand_at_range(rect[0], rect[0] + rect[2])
		y = self.get_rand_at_range(rect[1], rect[1] + rect[3])
		_tmp = win32api.MAKELONG(int(x), int(y))
		win32gui.PostMessage(self.hwnd, win32con.WM_MOUSEMOVE, 0, _tmp)
		time.sleep(0.002)
		win32gui.PostMessage(self.hwnd, win32con.WM_MOUSEMOVE, 0, _tmp)
		time.sleep(0.002)
		win32gui.PostMessage(self.hwnd, win32con.WM_MOUSEMOVE, 0, _tmp)

		return [x, y]

	def drag_p2p(self, pos1, pos2, drag_time=400):
		pass
	
	def drag_wayPoint(self, wayPoints, fine_move=True):
		tmp = win32api.MAKELONG(int(wayPoints[0][0]), int(wayPoints[0][1]))
		win32gui.PostMessage(self.hwnd, win32con.WM_LBUTTONDOWN, win32con.MK_LBUTTON, tmp)
		
		for wayPoint in wayPoints:
			tmp = win32api.MAKELONG(int(wayPoint[0]), int(wayPoint[1]))
			win32gui.PostMessage(self.hwnd, win32con.WM_MOUSEMOVE, 0, tmp)
			time.sleep(0.002)
		
		if fine_move:
			win32gui.PostMessage(self.hwnd, win32con.WM_MOUSEMOVE, 0, tmp)
			win32gui.PostMessage(self.hwnd, win32con.WM_MOUSEMOVE, 0, tmp)
			time.sleep(0.6)
			
		tmp = win32api.MAKELONG(int(wayPoints[0][0]), int(wayPoints[0][1]))
		win32gui.PostMessage(self.hwnd, win32con.WM_LBUTTONUP, win32con.MK_LBUTTON, tmp)
	
	def drag_rect(self, start_rect, end_rect, fine_move=True):
		start_x = self.get_rand_at_range(start_rect[0], start_rect[0] + start_rect[2])
		start_y = self.get_rand_at_range(start_rect[1], start_rect[1] + start_rect[3])
		end_x = self.get_rand_at_range(end_rect[0], end_rect[0] + end_rect[2])
		end_y = self.get_rand_at_range(end_rect[1], end_rect[1] + end_rect[3])
		start_pos = np.array([start_x, start_y])
		end_pos = np.array([end_x, end_y])
		wayPoints = []
		
		distance = np.linalg.norm(start_pos - end_pos)
		wayPoints.append(start_pos)
		cur_pos = start_pos
		while distance > MouseOperation.DRAG_DELTA:
			distance_delta = MouseOperation.DRAG_DELTA + self.get_rand_at_range(-MouseOperation.DRAG_DELTA_RANGE, MouseOperation.DRAG_DELTA_RANGE)
			direction_vector = end_pos - cur_pos
			theta = self.get_rand_at_range(-MouseOperation.DRAG_VECTOR_DELTA, MouseOperation.DRAG_VECTOR_DELTA)
			direction_vector = np.array(direction_vector / np.linalg.norm(direction_vector))
			direction_vector = rotate_via_numpy(direction_vector, theta)
			direction_vector *= distance_delta

			next_pos = np.floor(cur_pos + direction_vector)
			wayPoints.append(next_pos)
			cur_pos = next_pos
			distance = np.linalg.norm(cur_pos - end_pos)
			
		wayPoints.append(end_pos)
		self.drag_wayPoint(wayPoints, fine_move=fine_move)
		dx = end_x - start_x
		dy = end_y - start_y
		return dx, dy
	
	def send_backspace_key(self):
		win32gui.PostMessage(self.hwnd, win32con.WM_KEYDOWN, win32con.VK_F7, None)
		self.rnd_sleep(10)
	
	def send_key(self, pos, key_value=win32con.VK_F7):
		# win32con.VK_MBUTTON
		tmp_pos = win32api.MAKELONG(int(pos[0]), int(pos[1]))
		win32gui.PostMessage(self.hwnd, win32con.WM_MOUSEMOVE, 0, tmp_pos)
		win32gui.PostMessage(self.hwnd, win32con.WM_KEYDOWN, key_value, tmp_pos)
		self.rnd_sleep(200, 50)
		win32gui.PostMessage(self.hwnd, win32con.WM_MOUSEMOVE, 0, tmp_pos)
		win32gui.PostMessage(self.hwnd, win32con.WM_KEYUP, key_value, tmp_pos)

	def wheel_zoom(self, pos, direction="zoom_in"):
		tmp_pos = win32api.MAKELONG(int(pos[0]), int(pos[1]))
		win32gui.PostMessage(self.hwnd, win32con.WM_KEYDOWN, win32con.VK_CONTROL, None)
		self.rnd_sleep(250, 150)
		times = int(np.round(self.get_rand_at_range(7, 9)))
		if direction == "zoom_in":
			tmp = win32api.MAKELONG(8, 120)
			dir = 1
		else:
			tmp = win32api.MAKELONG(8, -120)
			dir = -1
		for i in range(times):
			win32gui.PostMessage(self.hwnd, win32con.MOUSE_WHEELED, tmp, tmp_pos)
			# win32api.mouse_event(win32con.MOUSEEVENTF_WHEEL, 0, 0, dir)
			self.rnd_sleep(30, 13)
		self.rnd_sleep(250, 150)
		win32gui.PostMessage(self.hwnd, win32con.WM_KEYUP, win32con.VK_CONTROL, None)
		
	def f5_key_zoom_out(self):
		win32gui.PostMessage(self.hwnd, win32con.WM_KEYDOWN, win32con.VK_F5, None)
		# win32gui.PostMessage(self.hwnd, win32con.WM_KEYDOWN, win32con.VK_CONTROL, None)
		self.rnd_sleep(50, 70)
		win32gui.PostMessage(self.hwnd, win32con.WM_KEYUP, win32con.VK_F5, None)
		# win32gui.PostMessage(self.hwnd, win32con.WM_KEYUP, win32con.VK_CONTROL, None)
	
	def press_change_app(self):
		win32gui.PostMessage(self.hwnd, win32con.WM_KEYDOWN, win32con.VK_F2, None)
		self.rnd_sleep(50, 70)
		win32gui.PostMessage(self.hwnd, win32con.WM_KEYUP, win32con.VK_F2, None)

def rotate_via_numpy(xy, radians):
	"""Use numpy to build a rotation matrix and take the dot product."""
	x, y = xy
	c, s = np.cos(radians), np.sin(radians)
	j = np.matrix([[c, s], [-s, c]])
	m = np.dot(j, [x, y])
	ret = np.array(m)[0]

	return ret


# test = MouseOperation("雷电模拟器", "TheRender")
# test.drag_rect([1200, 900, 50, 50], [1200, 250, 50, 50])
# while True:
# 	test.click_in_rect([560, 630, 235, 420])
# 	time.sleep(.3)
# test.driver.swipe(370, 250, 370, 900)
