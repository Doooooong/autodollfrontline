import DollFrontline as DF
import win32con
import datetime as dt

st_time = dt.datetime.now()
# 01/19 01:31 4059
# 01/20 01:51 4236
# 01/22 01:44 4420
# 01/25 01:07 4872


TIMES = 25  # 16*20  12-240000exp
# _times = input("Input times:")
# if _times == "":
# 	TIMES = 20
# else:
# 	TIMES = int(_times)

全部打手 = ["FAL", "G11", "416", "Zas", "SOPMODII", "M4A1", "AR15"]
# 打手 = ["G11", "FAL"]
打手 = ["416", "SOPMODII"]
fighter_slot = 2

df = DF.DollFrontlineBase()
df.AutoExtension = False  # 关闭自动扩编
zoom_count = 1

while TIMES > 0:
	NeedReturnToBase = False
	while df.getState() == ['']:
		df.op.rnd_sleep(100, 30)
	# for state_str in df.state:
	df.op.rnd_sleep(800, 80)
	if df.getState("战斗选择界面")[0] == "战斗选择界面":
		NeedReturnToBase = False
		# break
	else:
		NeedReturnToBase = True
	if NeedReturnToBase or df.SupportReturn:
		df.SupportReturn = False
		df.back_to_base()
		df.op.rnd_sleep(1000, 30)
		df.update_support_time()
		df.op.rnd_sleep(1000, 30)
		if df.TimeToSimulation == True:
			df.TimeToSimulation = False
			df.do_simulation()
		df.back_to_base()
		df.op.rnd_sleep(1000, 30)
		df.click_Battle()
	
	while True:
		if not df.select_enter_map(0, 2):
			continue
		#if not  continue
		if df.click_分解() == True:
			df.process_工厂()
			continue
		
		df.waiting_while("作战地图0_2", "state_confirm_1")
		# if df.isZoomOuted[0][2][0] == False:
		print("0-2放大", "zoom_confirm")
		if df.zoom_confirm("作战地图0_2", ["btn_司令部", "btn_机场", "btn_途经点1", "btn_终点"]) == False:
			# df.send_msg_to_phone()
			if df.confirm("0-2放大", "state_confirm_1"):
				zoom_count = 9
			df.zoom_out_init(zoom_count)
			zoom_count += 1
			
			# input("Zoom out, and enter anything to continue: ")
			df.click("作战地图0_2", "btn_Return", [1000, 200])
			continue
			# df.isZoomOuted[0][2][0] = True
		zoom_count = 0
		break
	
	while df.getState() == []:
		df.op.rnd_sleep(100, 30)
	df.op.rnd_sleep(1500, 300)
	df.click("作战地图0_2", "btn_司令部", [2000, 400])
	fighter_name = df.find_fighter(打手, fighter_slot)
	# df.click("战斗梯队部署界面", "btn_编成", [1800, 400])
	df.click_ex("战斗梯队部署界面", "btn_编成", [3300, 400], stuck_mode=[False, True],
				after_confirm=["编成界面", "state_confirm_1", None, True])
	
	M16A1_HP = df.get_HP(3)
	print("M16A1 HP is: ", M16A1_HP)
	# df.change_doll(2, fighter_name, ["AR"], ["5"])
	df.change_doll(fighter_slot, fighter_name)
	while df.getState("编成界面") == ["编成界面"]:
		df.click("编成界面", "btn_Return", [600, 400])
		df.op.rnd_sleep(100, 30)
	
	while df.getState("作战地图0_2") == []:
		df.op.rnd_sleep(100, 30)
	df.op.rnd_sleep(1000, 30)
	df.click_ex("作战地图0_2", "btn_司令部", [1200, 400], stuck_mode=[False, True]
	            , after_confirm=["战斗梯队部署界面", "state_confirm_1", None, True])
	if M16A1_HP < 35:
		df.repair_battle(3)
	df.click_ex("战斗梯队部署界面", "btn_Confirm", [1200, 400], stuck_mode=[False, True], after_confirm=["作战地图0_2", "state_confirm_1", None, True])
	
	df.click_ex("作战地图0_2", "btn_机场", [1200, 400], stuck_mode=[False, True], after_confirm=["战斗梯队部署界面", "state_confirm_1", None, True])
	df.click_ex("战斗梯队部署界面", "btn_Confirm", [1200, 400], stuck_mode=[False, True], after_confirm=["作战地图0_2", "state_confirm_1", None, True])
	
	df.click_ex("作战地图0_2", "btn_BattleStart", [3000, 400], stuck_mode=[False, True], click_cnt=2,after_confirm=["作战地图0_2", "btn_PlanExec", "btn_TurnEnd", True],)

	df.op.rnd_sleep(1 * 1000, 0)
	
	df.click("作战地图0_2", "btn_机场", [700, 200])  # 选中梯队
	df.click_ex("作战地图0_2", "btn_机场", [1400, 200], after_confirm=["战斗梯队部署界面", "btn_Confirm", None, True])  # 弹出补给界面
	df.click_ex("战斗梯队部署界面", "btn_补给", [1400, 200], after_confirm=["作战地图0_2", "state_confirm_1", None, True])
	df.click("作战地图0_2", "btn_司令部", [700, 200])  # 选中练级部队
	df.click("作战地图0_2", "btn_PlanMode", [700, 200])
	df.click("作战地图0_2", "btn_途经点1", [700, 200])
	df.click("作战地图0_2", "btn_终点", [700, 200])
	df.click("作战地图0_2", "btn_PlanExec", [700, 200])
	
	# df.op.rnd_sleep(140 * 1000, 0)
	battle_count = 0
	while True:
		# 战斗之间
		while not df.confirm("战斗中", "state_confirm_1"):
			df.op.rnd_sleep(2000, 300)
		# df.op.rnd_sleep(5000, 400)

		# 战斗中
		while df.confirm("战斗中", "state_confirm_1"):
			df.op.rnd_sleep(700, 300)
		
		battle_count += 1
		# 战斗结束，快速点击屏幕
		df.op.rnd_sleep(900, 100)
		for i in range(7):
			df.click_ex("战斗中", "btn_click", delay=[200, 30])
		if battle_count >= 5:
			break
		
	while not df.confirm("作战地图0_2", "btn_PlanMode", capture_rect_scale=1.05):
		df.op.rnd_sleep(2 * 1000, 0)
	# df.op.rnd_sleep(2 * 1000, 0)
	# while not df.confirm("作战地图0_2", "btn_PlanMode", capture_rect_scale=1.05):
	# 	df.op.rnd_sleep(2 * 1000, 0)
	df.op.rnd_sleep(1300, 200)
	df.click("作战地图0_2", "btn_PlanExec")  # 点击[结束]
	df.op.rnd_sleep(7 * 1000, 0)
	
	# while not df.confirm("作战完成", "state_confirm_1", capture_rect_scale=1.05):
	# 	df.op.rnd_sleep(1 * 1000, 0)
	while not df.confirm("作战完成", "state_confirm_2", capture_rect_scale=1.05):
		df.op.rnd_sleep(1 * 1000, 0)
	df.op.rnd_sleep(1 * 1000, 0)
	df.click("作战完成", "state_confirm_1", [300, 100], noCheck=True)
	df.click("作战完成", "state_confirm_1", [300, 100], noCheck=True)
	df.click("作战完成", "state_confirm_1", [300, 100], noCheck=True)
	df.click("作战完成", "state_confirm_1", [600, 100], noCheck=True)
	
	print("Times: ", TIMES, " finished!")
	print("======================================")
	TIMES -= 1
	df.stop_timeout = 0

df.send_msg_to_phone()
df.stop_timeout = -9999999999999
import 全力远征

st_time = dt.datetime.now()
print(dt.datetime.now() - st_time)
